{ open Parser }

let digit = ['0'-'9']
let letter = ['a'-'z' 'A'-'Z']
let exponent = ['e']['-']?digit+
let decimal = (digit+"."digit*)|(digit*"."digit+)
let stringable = ['!'-'~' ' ']

rule token = parse
  [' ' '\t' '\r' '\n'] { token lexbuf } (* Whitespace *)
| "/*"     { multi_comment lexbuf }     (* Multi-line Comments *)
| "//"     { single_comment lexbuf }    (* Single-line Comments *)
| '('      { LPAREN }
| ')'      { RPAREN }
| '['      { LBRACK }
| ']'      { RBRACK }
| '{'      { LBRACE }
| '}'      { RBRACE }
| ';'      { SEMI }
| ':'      { COLON }
| ','      { COMMA }
| '+'      { PLUS }
| '-'      { MINUS }
| '*'      { TIMES }
| '/'      { DIVIDE }
| '%'      { MOD }
| '='      { ASSIGN }
| ":="     { INIT }
| "=="     { EQ }
| "!="     { NEQ }
| '<'      { LT }
| '>'      { GT }
| "<="     { LEQ }
| ">="     { GEQ }
| "&&"     { AND }
| "||"     { OR }
| '!'      { NOT }
| "if"     { IF }
| "else"   { ELSE }
| "elif"   { ELSEIF }
| "for"    { FOR }
| "while"  { WHILE }
| "return" { RETURN }
| "break"  { BREAK }
| "continue" { CONTINUE }
| "true"   { BLIT(true) }
| "false"  { BLIT(false) }
| "func"   { FUNC }
| "include" { INCLUDE } (*For built-in functions*)
| "mat"    { TMAT }
| "bool"   { TBOOL } 
| "int"    { TINT }
| "float"  { TFLOAT }
| "string" { TSTRING }
| "void"   { TVOID }
| ['0'-'9']+ as lxm { ILIT(int_of_string lxm) }
| (decimal exponent?)|(digit+exponent) as lxm { FLIT(float_of_string lxm) }
| ['\"']stringable*['\"'] as lxm 
	{ SLIT((Scanf.unescaped (String.sub lxm 1 ((String.length lxm) - 2)))) }
| ['a'-'z' 'A'-'Z' '_']['a'-'z' 'A'-'Z' '0'-'9' '_']* as lxm { ID(lxm) }
| eof { EOF }
| _ as char { raise (Failure("illegal character " ^ Char.escaped char)) }


and multi_comment = parse
  "*/" { token lexbuf }
| _    { multi_comment lexbuf }

and single_comment = parse 
  '\n' { token lexbuf }
| _    { single_comment lexbuf }


(* Semantic checking for the Mpp compiler *)

open Ast
open Sast

module StringMap = Map.Make(String)

let debug_println (s: string) = 
	let debug = false in
	if debug then (print_string s)

type symbol_table = {
	(* Variables bound in current block *)
	variables: typ StringMap.t ref;
	(* Enclosing scope *)
	parent: symbol_table option ref;
}

let rec print_scope (scope: symbol_table): unit =
	debug_println "================\n";
	let rec helper (scope: symbol_table): unit =  
		StringMap.iter (fun k v -> debug_println ("(" ^ k ^ "," ^ string_of_typ v ^ ")\n")) !(scope.variables);
		debug_println "--------------\n";
		match !(scope.parent) with
		| Some(parent_scope) -> helper parent_scope
		| _ -> ()
	in helper scope; debug_println "================\n"

(* find variable from the current scope upto the basic/root scope *)
let rec find_variable (name: string) (scope: symbol_table): typ =
	try
		(* Try to find the binding in the nearest block *)
		StringMap.find name !(scope.variables)
	with Not_found ->
		match !(scope.parent) with
		| Some(parent_scope) -> find_variable name parent_scope
		| _ -> raise (Failure ("Variable \"" ^ name ^ "\" not found"))

(* add variable (name, t) to the current scope *)
let add_variable (name: string) (t: typ) (scope: symbol_table): unit =
	scope.variables := StringMap.add name t !(scope.variables)
	(*;
	debug_println ("\nafter add " ^ name ^ " :\n");
	print_scope scope
	*)


let check_dup_variable (name: string) (scope: symbol_table): unit = 
	try
		(* Try to find the binding in the nearest block *)
		ignore(StringMap.find name !(scope.variables));
		raise (Failure ("the variable \"" ^ name ^ "\" has already been defined in the current scope"))
	with Not_found -> ()


(* link the child scope to the parent scope and return the child scope,
	which is the current scope *)
let link_scope (child_scope: symbol_table) (parent_scope: symbol_table): symbol_table =
	child_scope.parent := Some parent_scope;
	child_scope

(* create a new scope, typical used with link scope *)
let new_scope (): symbol_table =
	let s = { variables = ref StringMap.empty; parent = ref None } in s

let is_func (t: typ): bool =
	match t with
	| Fun(_) -> true
	| _ -> false


let is_matrix (t: typ): bool =
	match t with
	| Matrix(_) -> true
	| _ -> false

let is_int_matrix (t: typ): bool =
	match t with
	| Matrix(Int, _) -> true
	| _ -> false

let is_bool_matrix (t: typ): bool =
	match t with
	| Matrix(Bool, _) -> true
	| _ -> false

let is_float_matrix (t: typ): bool =
	match t with
	| Matrix(Float, _) -> true
	| _ -> false


let is_int (t: typ): bool =
	match t with
	| Int -> true
	| _ -> false

let get_matrix_nr_dim (t: typ): int = 
	match t with
	| Matrix(_, li) -> List.length li
	| _ -> raise (Failure "not a matrix")

let rec get_list_of_len (length: int): int list = 
  match length with
  | 0 -> []
  | a -> -1::(get_list_of_len (length - 1))

let rec check_all_mone (li: int list): bool = 
	match li with
	| [] -> true
	| a::l -> (a = (-1)) && (check_all_mone l)

let is_matrix_fuzzy_equal (m1_t: typ) (m2_t: typ): bool =
	match (m1_t, m2_t) with
	| (Matrix(ele_ty1, li1), Matrix(ele_ty2, li2)) -> 
		if check_all_mone li1 || check_all_mone li2 then
			(ele_ty1 = ele_ty2) && (List.length li1 = List.length li2)
		else
			m1_t = m2_t
	| _ -> false

let is_func_fuzzy_equal (f1_t: typ) (f2_t: typ): bool =
	match (f1_t, f2_t) with
	| (Fun(name1, r_ty1, formal_ty_li1), Fun(name2, r_ty2, formal_ty_li2)) -> 
		if name1 = None || name2 = None then
			r_ty1 = r_ty2 && formal_ty_li1 = formal_ty_li2
		else
			f1_t = f2_t
	| _ -> false

let size_to_ele_typ (sz: expr): typ = 
	match sz with
	| IntLit(4) -> Int
	| IntLit(8) -> Float
	| IntLit(1) -> Bool
	| IntLit(i) -> raise (Failure ("no type with size" ^ (string_of_int i)))
	| _ -> raise (Failure "sadfa")

let rec check_sexpr_int_list (se_li: sexpr list) =
  match se_li with
  | [] -> ()
  | e::li -> 
    (match e with
    | (Int, _) -> check_sexpr_int_list li
	| (ty, _) -> raise (Failure ("Do not allow type " ^ (string_of_typ ty) ^ " in the matrix constructor.")))

let rec get_matrix_szs (nested_li: expr list): int list = 
	match nested_li with
	| (Mat(m_li))::n_li -> (List.length nested_li)::(get_matrix_szs m_li)
	| _ -> [List.length nested_li]

let rec get_matrix_ele_typ (nested_li: sexpr list): typ = 
	match nested_li with
	| (ty, SMat(m_li))::n_li -> get_matrix_ele_typ m_li
	| (ty, _)::n_li -> ty
	| _ -> raise (Failure "Not handled yet in get_matrix_ele_typ")

let rec check_matrix_typ_cons (ty: typ) (nested_li: sexpr list) : unit = 
	let ck (se: sexpr): unit = (match se with
	| (_, SMat(l)) -> check_matrix_typ_cons ty l
	| _ -> raise (Failure "fucking")) in
	match nested_li with
	| (_, SMat(_))::li as mat_list -> List.iter ck mat_list
	| (t, _)::li as other_list -> List.iter (fun (t, _) -> if t <> ty then raise (Failure "matrix type inconsistant")) other_list
	| _ -> raise (Failure "Not handled yet in check_matrix_typ_cons")

(* Return a function from our function table *)
let find_func (fname: string) (fmap: 'a StringMap.t): 'a =
	try StringMap.find fname fmap
	with Not_found -> raise (Failure ("unrecognized function " ^ fname))

let func_decl_to_func_type (f_decl: func_decl): typ = 
	let ty_li: typ list = List.map fst f_decl.formals in
	Fun((Some f_decl.fname, f_decl.rtype, ty_li))

let rec check_expr (e: expr) (scope: symbol_table) (fmap: 'a StringMap.t): sexpr =
	match e with
	| IntLit(a) -> (Int, SIntLit(a))
	| BoolLit(a) -> (Bool, SBoolLit(a))
	| FloatLit(a) -> (Float, SFloatLit(a))
	| StrLit(a) -> (String, SStrLit(a))
	| Var(name) -> 
		(
		try
			(find_variable name scope, SVar(name))
		with Failure(f) -> 
		(
			try
				(func_decl_to_func_type (find_func name fmap), SVar(name))
			with Failure(_) -> raise (Failure f)
		)
		)
	| Binop(e1, op, e2) as e ->
		let (t1, e1') = check_expr e1 scope fmap and (t2, e2') = check_expr e2 scope fmap in
		let err = "illegal binary operator " ^
		          string_of_typ t1 ^ " " ^ string_of_op op ^ " " ^
		          string_of_typ t2 ^ " in " ^ string_of_expr e
		in
		(* All binary operators require operands of the same type *)

		if t1 = t2 then
			(* Determine expression type based on operator and operand types *)
			let t = match op with 
			| Add | Sub | Mult | Div | Mod when t1 = Int -> Int
			| Add | Sub | Mult | Div | Mod when t1 = Float -> Float
			(* jierui TODO: need to do more checking when float and bool matrix is allowed *)
			(* jierui TODO: cannot mod when matrix is a float matrix *)
			| Add | Sub | Mult | Div | Mod when is_int_matrix(t1) -> t1
			| Add | Sub | Mult | Div | Mod when is_float_matrix(t1) -> t1
			| And | Or when is_bool_matrix(t1) -> t1
			(* jierui TODO: maybe we need to delete this operation *)
			| Add when t1 = String -> String
			(* jierui TODO: can this allow string? *)
			| Equal | Neq -> Bool
			| Less | Leq | Greater | Geq when t1 = Int || t1 = Float -> Bool
			| And | Or when t1 = Bool -> Bool
			(* TODO: If we want to do matrix-wise bool operation*)
			| _ -> raise (Failure err)
			in (t, SBinop((t1, e1'), op, (t2, e2')))
		else (
			(* jierui TODO: need to do more checking when float and bool matrix is allowed *)
			let t = match op with
			| Add | Sub | Mult | Div | Mod when is_int_matrix(t1) && t2 = Int -> t1
			| _ when is_int_matrix(t1) && t2 = Float -> raise (Failure err) 
			| Add | Sub | Mult | Div when is_float_matrix(t1) && t2 = Float -> t1
			| And | Or when is_bool_matrix(t1) && t2 = Bool -> t1
			| Add | Mult when t1 = Int && is_int_matrix(t2) -> t2
			| _ when t1 = Float && is_int_matrix(t2) -> raise (Failure err) 
			| Add | Mult when t1 = Float && is_float_matrix(t2) -> t2
			| And | Or when t1 = Bool && is_bool_matrix(t2) -> t2
			| _ -> raise (Failure err)
			in (t, SBinop((t1, e1'), op, (t2, e2')))
		) 
	| Unop(op, ee) as e ->
		let (t, ee') = check_expr ee scope fmap in
		let err = "illegal unary operator " ^ string_of_uop op ^ " " ^ string_of_typ t  ^ " in " ^ string_of_expr e
		in (match (t, op) with
		| (Bool, Not) | (Int, Neg) | (Float, Neg) -> (t, SUnop(op, (t, ee')))
		| _ -> raise (Failure err))
	| Mat(e_li) ->
		(* the size type of the matrix is checked and determined in parser, not jagged *)
		(* but need to check the element type consistency *)
		let checked_list: sexpr list = 
			List.map ((fun scope fmap e -> check_expr e scope fmap) scope fmap) e_li in
		let mat_ele_ty: typ = get_matrix_ele_typ checked_list in
		let _ = check_matrix_typ_cons mat_ele_ty checked_list in
		(Matrix(mat_ele_ty, get_matrix_szs e_li), SMat(checked_list))
		(* TODO: type checking and get type(replace that Pending), remove the type checking in parser *)
	| GetIndex(le, ri) ->
		(* make sure le is a Matrix, ri is an Int *)
		let (le_ty, le_se) = check_expr le scope fmap and (ri_ty, ri_se) = check_expr ri scope fmap in
		(match (le_ty, ri_ty) with
		| _ when not (is_matrix le_ty) -> raise (Failure "cannot get index from type other than matrix")
		| _ when not (is_int ri_ty) -> raise (Failure "index value must be an int")
		| _ -> 
			let temp = SGetIndex((le_ty, le_se), (ri_ty, ri_se)) in
			(match le_ty with
			| Matrix(ele_typ, a::[]) -> (ele_typ, temp)
			| Matrix(ele_typ, a::b::li) -> (Matrix(ele_typ, b::li), temp)
			| _ -> raise (Failure "error at line 103")))
	| GetSlice(le, sl_e) ->
		let err = "must ints inside the slice expr" in
		let err2 = "cannot get slice from type other than matrix" in
		let ssl_e =
			(match sl_e with
			| (None, None) -> (None, None)
			| (Some(e1), None) ->
				let (ty, se) = check_expr e1 scope fmap in
				if ty = Int then (Some((ty, se)), None) else raise (Failure err)
			| (None, Some(e2)) ->
				let (ty, se) = check_expr e2 scope fmap in
				if ty = Int then (Some((ty, se)), None) else raise (Failure err)
			| (Some(e1), Some(e2)) ->
				let (ty1, se1) = check_expr e1 scope fmap and (ty2, se2) = check_expr e2 scope fmap in
				if ty1 = Int && ty2 = Int then (Some((ty1, se1)), Some((ty2, se2))) else raise (Failure err))
		in
		let (ty, se) = check_expr le scope fmap in
		if is_matrix ty then (ty, SGetSlice((ty, se), ssl_e)) else raise (Failure err2)
	| Assign(e1, e2) ->
		let (ty1, se1) = check_expr e1 scope fmap and (ty2, se2) = check_expr e2 scope fmap in
		if ty1 <> ty2 then
		    let err = "Cannot assign a " ^ string_of_typ ty2 ^ " to " ^ string_of_typ ty1 in
			raise (Failure err)
		else
			(ty1, SAssign((ty1, se1), (ty2, se2)))
	| Init(t, s, e) -> (* when t is Matrix(_), it is checked in parser so we do not need to check it again *)
		let (ty, se) = check_expr e scope fmap in
		let err = "Cannot init a " ^ string_of_typ ty ^ " to " ^ string_of_typ t in
		(match t with (* TODO: size check? *)
		| Matrix(Pending, szs) -> check_dup_variable s scope; add_variable s ty scope; (ty, SInit(s, (ty, se)))
		| Matrix(ty', szs) -> if not (is_matrix_fuzzy_equal t ty) then raise (Failure err) else begin check_dup_variable s scope; add_variable s ty scope; (ty, SInit(s, (ty, se))) end
		| Fun(None, _, _) -> if not (is_func_fuzzy_equal t ty) then raise (Failure err) else begin check_dup_variable s scope; add_variable s ty scope; (ty, SInit(s, (ty, se))) end
		| Pending -> check_dup_variable s scope; add_variable s ty scope; (ty, SInit(s, (ty, se)))
		| _ (* Int, FLoat, String  -> *) -> if t <> ty then raise (Failure err) else begin check_dup_variable s scope; add_variable s ty scope; (ty, SInit(s, (ty, se))) end)
	| Call("printf", args_li) -> 
		let arg': sexpr list = 
			(List.map ((fun scope fmap e -> check_expr e scope fmap) scope fmap) args_li) in
		(match arg' with
		| [] -> raise (Failure "receiving 0 argument in printf()")
		| (ty, _)::li when ty = String -> (Int, SCall("printf", arg'))
		| (ty, _)::li when ty <> String -> 
		raise (Failure ("illegal argument found " ^ string_of_typ ty ^ ", but expected string in " ^ string_of_expr e))
		| _ -> raise (Failure "never run here"))
	| Call("print_matrix", args_li) as call ->
		let arg': sexpr list = 
			(List.map ((fun scope fmap e -> check_expr e scope fmap) scope fmap) args_li) in
		let len: int = List.length arg' in
		(match len with
		| _ when len <> 1 -> 
			raise (Failure ("receiving " ^ string_of_int len ^ " arguments in " ^ string_of_expr call))
		| _ when not (is_matrix (fst (List.hd arg'))) -> 
			raise (Failure ("illegal argument found " ^ string_of_typ (fst (List.hd arg')) 
				^ ", but expected Mat in " ^ string_of_expr (List.hd args_li)))
		| _ -> (Void, SCall("print_matrix", arg')))
	| Call("init_mat", args_li) -> 
		let ele_ty: typ = size_to_ele_typ (List.hd args_li) in
		(* all elements in args_li are int, which are checked in parser *)
		let arg': sexpr list = List.map ((fun scope fmap e -> check_expr e scope fmap) scope fmap) args_li in
		let _ = check_sexpr_int_list arg' in
		let sz_li: int list = get_list_of_len (List.length (List.tl arg')) in
		(Matrix(ele_ty, sz_li), SCall("init_mat", arg'))
	| Call("get_dim", args_li) as call -> 
		let arg': sexpr list = List.map ((fun scope fmap e -> check_expr e scope fmap) scope fmap) args_li in
		let len: int = List.length arg' in
		(match len with
		| _ when len <> 1 -> 
			raise (Failure ("receiving " ^ string_of_int len ^ " arguments in " ^ string_of_expr call))
		| _ when not (is_matrix (fst (List.hd arg'))) -> 
			raise (Failure ("illegal argument found " ^ string_of_typ (fst (List.hd arg')) 
				^ ", but expected Mat in " ^ string_of_expr (List.hd args_li)))
		| _ -> 
		 	(Matrix(Int, (get_matrix_nr_dim (fst (List.hd arg')))::[]), SCall("get_dim", arg')))
	| Call(f_name, args_li) as call ->
		(* TODO: need to consider how to support the higher order function *)
		let _ = debug_println ("function name:" ^ f_name) in
        let fd = 
			try
				find_func f_name fmap
			with
			| _ -> 
			(match find_variable f_name scope with
			| Fun(Some(ss), r_ty, for_ty_li) -> debug_println ("find:" ^ ss); find_func ss fmap
			| Fun(None, r_ty, for_ty_li) -> 
				let for_ty_li_to_formals (for_ty_li: typ list): (typ * string) list = 
				List.map (fun a -> (a, "")) for_ty_li in
				{ rtype = r_ty; fname = ""; formals = (for_ty_li_to_formals for_ty_li); body = []}
			| _ -> raise (Failure ("unrecognized variable " ^ f_name))) 
    	in
        let param_length = List.length fd.formals in
        if List.length args_li != param_length then
			raise (Failure ("expecting " ^ string_of_int param_length ^
				" arguments in " ^ string_of_expr call))
        else
            begin
                let check_call ((f_ty, _): typ * string) (e: expr): sexpr =
                    let (ty, se) = check_expr e scope fmap in
                    let err = "illegal argument found " ^ string_of_typ ty ^
                             ", but expected " ^ string_of_typ f_ty ^ " in " ^ string_of_expr e
                    in 
                    (match err with
                    | _ when is_matrix f_ty -> if is_matrix_fuzzy_equal f_ty ty then (ty, se) else raise (Failure err)
                    | _ when is_func f_ty -> if is_func_fuzzy_equal f_ty ty then (ty, se) else raise (Failure err)
                    | _ -> if f_ty = ty then (ty, se) else raise (Failure err))
                in
                let args': sexpr list = List.map2 check_call fd.formals args_li
                in (fd.rtype, SCall(f_name, args'))
            end
(*    | _ -> raise (Failure "no such expr")
*)
(* check if this expr has type bool, typically used in IF FOR WHILE statments *)
let check_bool_expr (e: expr) (scope: symbol_table) (fmap: 'a StringMap.t): sexpr =
	let (t, e') = check_expr e scope fmap in
	match t with
	| Bool -> (t, e')
	|  _ -> raise (Failure ("expected Boolean expression in " ^ string_of_expr e))


let check_func (f_decl: func_decl) (scope': symbol_table) (fmap': 'a StringMap.t): sfunc_decl =
	(* TODO: check duplicate names in function foramls *)

	let rec check_stmt_list (l: stmt list) (scope: symbol_table) (fmap: 'a StringMap.t): sstmt list =
		List.map ((fun scope fmap st -> check_stmt st scope fmap) scope fmap) l
	(*
		match l with
		| [] -> []
		| s :: sl -> (check_stmt s scope fmap) :: (check_stmt_list sl scope fmap)
	
		cannot flatten the block because of local scope
		| Block sl :: sl'  -> check_stmt_list (sl @ sl')
	*)
	and check_stmt (s: stmt) (scope: symbol_table) (fmap: 'a StringMap.t): sstmt =
		match s with
		| Block(sl) ->
			(* program runs into a new block, indicating it get inside a new local scope *)
			SBlock(check_stmt_list sl (link_scope (new_scope ()) scope) fmap)
		| Expr(e) -> SExpr(check_expr e scope fmap)
		| If(e, s1, s2) ->
			SIf(check_bool_expr e scope fmap, check_stmt s1 scope fmap, check_stmt s2 scope fmap)
		| While(e, s1) ->
			SWhile(check_bool_expr e scope fmap, check_stmt s1 scope fmap)
		(* return statement, need to compare the current return type with expected return type *)
		| Return(None) ->
			if f_decl.rtype = Void then SReturn(None)
			else raise (Failure ("the return type is void now, but expected " ^ string_of_typ f_decl.rtype ^ " in return;"))
		| Return(Some e) ->
		    let (t, e') = check_expr e scope fmap in
	        if t = f_decl.rtype 
	        || (is_matrix_fuzzy_equal t f_decl.rtype) 
	        || (is_func_fuzzy_equal t f_decl.rtype) then SReturn(Some(t, e'))
	        else raise (
	            Failure ("the return type is " ^ string_of_typ t ^ " now, but expected " ^ string_of_typ f_decl.rtype ^ " in " ^ string_of_expr e))
	(* TODO:
		| For (e1, e2, e3, s1) -> SFor (check_expr e1, check_expr e2, check_expr e3, check_stmt s1)
	*)
		| Break -> SBreak
		| Continue -> SContinue
		| _ -> raise (Failure "Not handled")
	in
	{
		srtype = f_decl.rtype;
    	sfname = f_decl.fname;
    	sformals = f_decl.formals;
    	sbody = check_stmt_list f_decl.body scope' fmap'
	}


let get_fun_scope (parent_scope: symbol_table) (f_decl: func_decl): symbol_table = 
	let fun_scope: symbol_table = new_scope () in
	List.iter (fun (t, n) -> add_variable n t fun_scope) f_decl.formals;
	link_scope fun_scope parent_scope

let get_name_from_includ (inc: includ): string =
	match inc with
	| FileIncl(s) -> s

let get_id_from_init_expr (e: expr): string =
	match e with
	| Init(_, s, _) -> s
	| _ -> raise (Failure "The input expr is not Init.")

let get_name_from_func_decl (f_decl: func_decl): string =
	f_decl.fname

let get_string_list (l: 'a list) (extract: 'a -> string): string list =
	List.fold_left (fun li b -> (extract b)::li) [] l

(* check duplicate in names *)
let check_dup_names (kind: string) (names: string list): unit =
    let rec dups li =
	    match li with
		 [] -> ()
		| (a :: b :: _) when a = b ->
			raise (Failure ("duplicate names in " ^ kind ^ ": " ^ a))
		| _ :: li -> dups li
    in dups (List.sort (fun a b -> compare a b) names)

(* Add function to function table *)
let add_func (fmap: 'a StringMap.t) (fd: func_decl) : 'a StringMap.t =
	match fd with (* No duplicate functions or redefinitions of built-ins *)
	  _ when StringMap.mem fd.fname fmap ->
	  	raise (Failure ("duplicate function " ^ fd.fname))
	| _ ->  StringMap.add fd.fname fd fmap

let check ((incs, g_vars, funcs): program) : sprogram =
	let checked_incs: string list = (get_string_list incs get_name_from_includ) in
	(* check duplicate in file includes *) (* TODO *)
	check_dup_names "file inculdes" checked_incs;
	(* check duplicate in global variables *)
	check_dup_names "global variables" (get_string_list g_vars get_id_from_init_expr);
	(* check duplicate in functions *)
	(* TODO: only check the duplictate name,
	cannot distiguish functions that have same name but different parameters *)
	check_dup_names "functions" (get_string_list funcs get_name_from_func_decl);

	let blist: func_decl list = [
		(*{ rtype = Pending; fname = "init_mat"; formals = []; body = [] };*)
		{ rtype = Pending; fname = "get_dim"; formals = []; body = [] };
		{ rtype = Int; fname = "printf"; formals = []; body = [] };
		{ rtype = Void; fname = "print_matrix"; formals = []; body = [] };
		{ rtype = Void; fname = "assert"; formals = [(Bool, "b")]; body = []};
		{ rtype = Void; fname = "assert_msg"; formals = [(String, "str"); (Bool, "b")]; body = []}
	] in

	(* global_scope is the basic/root scope of this program, it is the ancestor of other scopes *)
	let global_scope = new_scope () in
	(* TODO: built-in functions *)
	let bfmap: func_decl StringMap.t = List.fold_left (fun m b -> StringMap.add b.fname b m) StringMap.empty blist in
	(* add all functions into the fmap *)
	let fmap = List.fold_left add_func bfmap funcs in
	(* Ensure "main" is defined *)
	(* check all global variables: g_vars -> checked_g_vars *)
	let checked_g_vars: sexpr list =
		List.map (fun b -> (check_expr b global_scope fmap)) g_vars in
	let _ = find_func "main" fmap in
	let checked_funcs: sfunc_decl list =
		List.fold_left (fun li b -> (check_func b (get_fun_scope global_scope b) fmap)::li) [] funcs in
	(checked_incs, checked_g_vars, checked_funcs)

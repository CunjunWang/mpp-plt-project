open Sast

(* used when only want to test a single statement *)
let append_dummy_main p = p ^ "\n int func main() { return 0; } \n"

let test_semant (source : string) (has_main : bool) : unit =
  try
    let full_program = if has_main = false then append_dummy_main source else source in
    let lexbuf = Lexing.from_string full_program in
    let program = Parser.program Scanner.token lexbuf in
    let sprogram = Semant.check program in
    print_endline (string_of_sprogram sprogram)
  with
    | Not_found -> print_endline ("Variable not found.\n")
    | Invalid_argument(s) | Failure(s) -> print_endline (s ^ "\n")
    | _ -> print_endline ("Unexpected parser error in test semant, check the statements or go back to debug parser!\n")
;;

print_endline "1. TEST INITIALIZATION. \n";;

print_endline "1.1. test integer init correctly.";;
test_semant "int a = 1;\n" false;;

print_endline "1.2. test integer init incorrectly.";;
test_semant "int a = \"123\";" false;;

print_endline "1.3. test string init correctly.";;
test_semant "string a = \"1\\n23\";" false;;

print_endline "1.4. test float init correctly.";;
test_semant "float a = 1.0;" false;;

print_endline "1.5. test float init with int (error).";;
test_semant "float a = 1;" false;;

print_endline "1.6. test matrix init with correct type.";;
test_semant "mat<int, 1> a = [1];" false;;

print_endline "1.7. test matrix init with incorrect type.";;
test_semant "mat<int, 1> a = [\"s\"];" false;;

print_endline "1.8. test matrix init with incorrect dimension.";;
test_semant "mat<int, 2> a = [1, 2];" false;;

print_endline "1.9. test matrix init with correct dimension.";;
test_semant "mat<int, 1> a = [1, 2];" false;;

print_endline "1.10. test matrix init with correct dimension.";;
test_semant "mat<int, 2> a = [[1, 2], [3,4]];" false;;

print_endline "1.11. test matrix init with incorrect dimension.";;
test_semant "mat<int, 2> a = [[1, 2], [3]];" false;;

print_endline "1.12. test bool init correctly.";;
test_semant "bool a = true;" false;;

print_endline "1.13. test bool init incorrectly.";;
test_semant "bool a = \"a\";" false;;

print_endline "1.14. test matrix init with consistent type.";;
test_semant "mat<float, 2> a = [[1., 0.7+.2], [3e5, 8e4]];" false;;

print_endline "1.15. test matrix init with inconsistent type.";;
test_semant "mat<int, 2> a = [[1, 2], [3, 0.9]];" false;;


print_endline "2. TEST RETURN TYPE. \n";;

print_endline "2.1. test return type match.";;
test_semant ("string a = \"a\"; \n string func main() { \n return a; \n }") true;;

print_endline "2.2. test return type does not match.";;
test_semant ("string a = \"a\"; \n int func main() { \n return a; \n }") true;;

print_endline "2.3. test return type void is valid.";;
test_semant ("void func main() { \n return; \n }") true;;

print_endline "2.4. test void function returns a expression (error).";;
test_semant ("int a = 1; void func main() { \n return a; \n }") true;;



print_endline "3. TEST SCOPE. \n";;

print_endline "3.1. test can see global variable in function.";;
test_semant ("int a = 1; int func main() { \n return a; \n }") true;;

print_endline "3.2. test local var can replace global var with the same name.";;
test_semant ("int a = 1; string func main() { \n string a = \"123\"; \n return a; \n }") true;;

print_endline "3.3. test variable is bounded by block. (should pass)";;
test_semant ("int func main() { \n" ^
             "  int i = 0; \n" ^
             "  { \n" ^
             "    int i = 1; \n" ^
             "    int j = 2; \n" ^
             "  } \n" ^
             "}") true;;

print_endline "3.4. test variable is bounded by block. (should fail)";;
test_semant ("int func main() { \n" ^
             "  int i = 0; \n" ^
             "  { \n" ^
             "    int i = 1; \n" ^
             "    int j = 2; \n" ^
             "  } \n" ^
             "  int k = j; \n" ^
             "}") true;;

print_endline "3.5. test should be able to declare new var in new scope even with same name.";;
test_semant ("int func main() { \n" ^
             "  int i = 0; \n" ^
             "  { \n" ^
             "    string i = \"123\"; \n" ^
             "    string j = i; \n" ^
             "  } \n" ^
             "}") true;;

print_endline "3.6. test should be able to define function.";;
test_semant ("int func sum(int a, int b) { \n" ^
             " int sum = a + b; \n" ^
             " return sum; \n" ^
             "}") false;;

print_endline "3.7. test should be able to define function without intermeidate expression.";;
test_semant ("int func sum(int a, int b) { \n" ^
             " return a + b; \n" ^
             "}") false;;

print_endline "3.8. test should be able to call self-defined function.";;
test_semant ("int func test1() { \n" ^
             " return 1; \n" ^
             "} \n" ^
             "\n" ^
             "int a = test1(); \n" ^
             "\n" ^
             "int func main() { \n" ^
             "  return a; \n" ^
             "}") true;;

print_endline "3.9. test should be able to call self-defined function to assign locals.";;
test_semant ("int func test1() { \n" ^
             " return 1; \n" ^
             "} \n" ^
             "int func main() { \n" ^
             "  int a = test1(); \n" ^
             "  return a; \n" ^
             "}") true;;

print_endline "3.10. test duplicate definition (error)";;
test_semant ("int func main() { \n" ^
             "  int i = 0; \n" ^
             "  { \n" ^
             "    int i = 1; \n" ^
             "    int i = 2; \n" ^
             "  } \n" ^
             "}") true;;

print_endline "3.11. test recursive function";;
test_semant ("mat<bool, 1> visited = [false];\n" ^
             "\n" ^
             "int func dfs(int v) {\n" ^
             "  visited[0] = true;\n" ^
             "  dfs(v);\n" ^
             "}\n") false;;

print_endline "3.12. test duplicate function definition";;
test_semant ("int func main() {\n" ^
             "  return 1 + 2;\n" ^
             "}\n") false;;

print_endline "3.13. test scope in control flow";;
test_semant ("int func binary_search(int x, mat<int, 1> arr) {\n" ^
             "  int lo = 0; \n" ^
             "  int hi = 5; \n" ^
             "  int mid = lo + (hi - lo) / 2; \n" ^
             "  while (lo <= hi) { \n" ^
             "    if (arr[mid] == x) { \n" ^
             "      return mid; \n" ^
             "    } \n" ^
             "    if (arr[mid] < x) { \n" ^
             "      lo = mid + 1; \n" ^
             "    } else { \n" ^
             "      hi = mid - 1; \n" ^
             "    } \n" ^
             "  } \n" ^
             "  return -1; \n" ^
             "}\n") false;;

(* TODO: this one fails *)
print_endline "3.14. test should detect a missing return statement.";;
test_semant ("int func binary_search(int x, mat<int, 1> arr) {\n" ^
             "  int lo = 0; \n" ^
             "  int hi = 5; \n" ^ (* TODO: replace by arr.length *)
             "  int mid = lo + (hi - lo) / 2; \n" ^
             "  while (lo <= hi) { \n" ^
             "    if (arr[mid] == x) { \n" ^
             "      return mid; \n" ^
             "    } \n" ^
             "    if (arr[mid] < x) { \n" ^
             "      lo = mid + 1; \n" ^
             "    } else { \n" ^
             "      hi = mid - 1; \n" ^
             "    } \n" ^
             "  } \n" ^
             "}\n") false;;



print_endline "4. TEST Matrix Initialization. \n";;

print_endline "4.1 test matrix initialization";;
test_semant ("/* this is a matrix */" ^
              "m := mat(int, 3, 4);") false;;

print_endline "4.2 test matrix initialization";;
test_semant ("/* this is a matrix */" ^
              "mat<int, 2> m = mat(int, 3, 4);") false;;

print_endline "4.3 test matrix initialization";;
test_semant ("/* this is a matrix */" ^
              "m := mat(bool, 3, 4, 5);") false;;

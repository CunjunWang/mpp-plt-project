type typ = Pending | Void | Bool | Int | Float | String | Matrix of typ * int list | Fun of string option * typ * typ list
(* TODO: type function for higher order function *)

type op = Add | Sub | Mult | Div | Equal | Neq | Less | Leq | Greater | Geq|
		  And | Or | Mod

type uop = Neg | Not

(* 
slice_expr: expression inside [] to slice the matrix 
[:] => (None, None)
[:expr] => (None, Some expr)
*)
type slice_expr = expr option * expr option
and expr = 
  IntLit of int
| BoolLit of bool
| FloatLit of float
| StrLit of string
| Var of string
| Binop of expr * op * expr
| Unop of uop * expr
| Call of string * expr list
(* Assignment uses operator = *)
| Assign of expr * expr
(* Initialization uses operator := *)
| Init of typ * string * expr
(* | Typinit of typ * string * typ * expr list *)
(* [] => Mat([Mat([])]) empty matrix (TODO: 1*0 ????) *)
(* [;] => Mat([Mat([]);Mat([])]) 2*0 matrix *)
(* [1,2;3,4] => Mat([Mat([IntLit(1);IntLit(2)]),Mat([IntLit(3);IntLit(4)])]) 2*2 matrix *)
(* TODO: currently, the matrix's size should be fixed and the size of each row should be same *)
| Mat of expr list
(* 
a := [1,2;3,4];
a[0][1];
 => 
GetIndex(
	GetIndex(
		Mat([Mat([IntLit(1);IntLit(2)]),Mat([IntLit(3);IntLit(4)])]), 
		IntLit(0)
	), 
	IntLit(1)
)
*)
| GetIndex of expr * expr
(* 
a := [1,2;3,4];
a[0:][:1];
 => 
GetSlice(
	GetSlice(
		Mat([Mat([IntLit(1);IntLit(2)]),Mat([IntLit(3);IntLit(4)])]), 
		(IntLit(0), None)
	), 
	(None, IntLit(1))
)
*)
| GetSlice of expr * slice_expr

type stmt = 
  Block of stmt list
| Expr of expr
| Return of expr option
| If of expr * stmt * stmt
| For of expr * expr * expr * stmt (* TODO: expr option *)
| While of expr * stmt
| Break
| Continue

type func_decl = {
	rtype: typ; (* return type *)
	fname: string; (* function name *)
	formals: (typ * string) list; (* function parameters *)
	body: stmt list; (* function body *)
}

type includ = FileIncl of string

(* (include files, global variables, functions) *)
type program = includ list * expr list * func_decl list

(* Pretty-printing funcitons *)
let string_of_include (inc: includ): string =
  match inc with
    FileIncl(file) -> "#include <" ^ file ^ ">"

let string_of_op (a: op): string = 
  match a with
    Add -> "+"
  | Sub -> "-" 
  | Mult -> "*"
  | Div -> "/"
  | Equal -> "=="
  | Neq -> "!="
  | Less -> "<"
  | Leq -> "<="
  | Greater -> ">"
  | Geq -> ">="
  | And -> "&&"
  | Or -> "||"
  | Mod -> "%"

let string_of_uop (a: uop): string = 
  match a with
    Neg -> "-"
  | Not -> "!"

let rec string_of_typ (ty: typ): string = 
  match ty with
    Pending -> "pending"
  | Void -> "void" 
  | Bool -> "bool" 
  | Int -> "int"
  | Float -> "float"
  | Matrix (ty, intlist) -> "Matrix<" ^ (string_of_typ ty) ^ ", [" ^ ( String.concat "," (List.map string_of_int intlist)) ^ "]>"
  | String -> "string"
  | Fun (Some(s),ty,tylist)-> "("^ s ^ ") func "^(string_of_typ ty) ^ " (" ^ (String.concat "," (List.map string_of_typ tylist)) ^ ")" 
  | Fun (None,ty,tylist)-> "(no-binding) func "^(string_of_typ ty) ^ " (" ^ (String.concat "," (List.map string_of_typ tylist)) ^ ")" 

let rec string_of_expr e: string = 
  match e with
  | IntLit(num) -> string_of_int num
  | BoolLit(true) -> "true"
  | BoolLit(false) -> "false"
  | FloatLit(fnum) -> string_of_float fnum
  | StrLit(str) -> str
  | Var(vname) -> vname
  | Binop(e1, o, e2) -> string_of_expr e1 ^ " " ^ string_of_op o ^ " " ^ string_of_expr e2
  | Unop(uop, e) -> string_of_uop uop ^ string_of_expr e
  | Call(f, el) -> f ^ "(" ^ String.concat "," (List.map string_of_expr el) ^ ")"
  | Assign(e1, e2) -> string_of_expr e1 ^ " = " ^ string_of_expr e2
  | Init(typ, vname, e) -> string_of_typ typ ^ " " ^  vname ^ " = " ^ string_of_expr e ^ ";\n"
  (* | Typinit (typ1,vname,typ2,el)->string_of_typ typ1 ^" "^vname ^ "=" ^ string_of_typ typ2 ^ "(" ^ String.concat "," (List.map string_of_expr el) ^ ") ;\n" *)
  | Mat(el) -> "[" ^ String.concat "," (List.map string_of_expr el) ^ "]"
  | GetIndex(e1, e2) -> string_of_expr e1 ^ "[" ^ string_of_expr e2 ^ "]"
  | GetSlice(e, se) -> string_of_expr e ^ "[" ^ (string_of_opt (fst se)) ^ ":" ^ (string_of_opt (snd se)) ^ "]"
and string_of_opt (opt: expr option): string = 
  match opt with
  | None -> "NONE"
  | Some(e) -> string_of_expr e


let rec string_of_stmt (st: stmt): string =
  match st with 
    Block(stmts)->
    "{\n"^ String.concat "" (List.map string_of_stmt stmts) ^ ";}\n" 
  | Expr(e) -> string_of_expr e ^ ";\n"
  | Return(e) -> "return " ^ string_of_opt e ^ ";\n"
  | If(e, s1, s2) -> "if (" ^ string_of_expr e ^ ")\n" ^ string_of_stmt s1 ^ "else\n" ^ string_of_stmt s2
  | For(e1, e2, e3, s) -> "for (" ^ string_of_expr e1 ^ ";" ^ string_of_expr e2 ^ ";" ^ string_of_expr e3 ^ ")" ^ string_of_stmt s (* TODO: expr option *)
  | While(e, s) -> "while (" ^ string_of_expr e ^ ") " ^ string_of_stmt s
  | Break -> "break;"
  | Continue -> "continue;"

let string_of_fdecl fdecl =
  string_of_typ fdecl.rtype ^ " " ^
  fdecl.fname ^ "(" ^ String.concat ", " (List.map (fun a ->(string_of_typ (fst a)  ^" "^ (snd a))) fdecl.formals) ^
  ")\n{\n" ^
  String.concat "" (List.map string_of_stmt fdecl.body) ^
  "}\n"

let string_of_program (inc, vars, funcs) =
  "\n\nParsed program: \n\n" ^
  String.concat "" (List.map string_of_include inc) ^ "\n" ^
  String.concat "" (List.map string_of_expr vars) ^ "\n" ^
  String.concat "\n" (List.map string_of_fdecl funcs)

#include "mat.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

const bool DEBUG = false;

/*
struct Mat {
	int nr_dim;
	int ele_ty;
	int* dim_szs;
	void* start; // point to the first int element
	int length;
};
*/

void assert(bool b) {
	if (!b) {
		printf("Assert fail!\n");
		exit(EXIT_FAILURE);
	}
}

void assert_msg(void* str, bool b) {
	if (!b) {
		printf("%s\n", (char*)str);
		exit(EXIT_FAILURE);
	}
}

void print_int_mat(int* start,int dim,int*dim_szs,int length){
	if (dim<1) return;
	printf("[");
	if (dim==1){
		for (int i=0;i<dim_szs[0];++i){
			if (i>0) printf(",");
			printf("%d",*(start+i));
		}
	}
	else{
		for (int i=0;i<dim_szs[0];++i){
			if (i>0)
				printf(",");
			print_int_mat(start+i*(length/dim_szs[0]),dim-1,dim_szs+1,length/dim_szs[0]);
		}
	}
	printf("]");
}

void print_double_mat(double* start,int dim,int*dim_szs,int length){
	if (dim<1) return;
	printf("[");
	if (dim==1){
		for (int i=0;i<dim_szs[0];++i){
			if (i>0) printf(",");
			printf("%f",*(start+i));
		}
	}
	else{
		for (int i=0;i<dim_szs[0];++i){
			if (i>0)
				printf(",");
			print_double_mat(start+i*(length/dim_szs[0]),dim-1,dim_szs+1,length/dim_szs[0]);
		}
	}
	printf("]");
}

void print_bool_mat(bool* start,int dim,int*dim_szs,int length){
	if (dim<1) return;
	printf("[");
	if (dim==1){
		for (int i=0;i<dim_szs[0];++i){
			if (i>0) printf(",");
			printf("%d",*(start+i));
		}
	}
	else{
		for (int i=0;i<dim_szs[0];++i){
			if (i>0)
				printf(",");
			print_bool_mat(start+i*(length/dim_szs[0]),dim-1,dim_szs+1,length/dim_szs[0]);
		}
	}
	printf("]");
}

void print_matrix(struct Mat* mat) {
	//2*3*2 [[[1,2],[3,4],[5,6]],[[1,2],[3,4],[5,6]]]
	switch(mat->ele_ty) {
		case 4:
			print_int_mat((int*)mat->start,mat->nr_dim,mat->dim_szs,mat->length);
			break;
		case 8:
			print_double_mat((double*)mat->start,mat->nr_dim,mat->dim_szs,mat->length);
			break;
		case 1:
			print_bool_mat((bool*)mat->start,mat->nr_dim,mat->dim_szs,mat->length);
			break;
		default:
		break;
	}
	printf("\n");

}

void init_mat(struct Mat* mat, int n, ...) {
	if (n - 1 <= 0) {
		printf("Cannot initialize the matrix. The dimension <= 0.\n");
		exit(EXIT_FAILURE);
	}

	int sum = 0;
	int total = 1;

	va_list valist;
	va_start(valist, n);

	mat->nr_dim = n - 1;

	mat->dim_szs = (int*) malloc(sizeof(int) * mat->nr_dim);
	if (mat->dim_szs == NULL) {
		printf("Cannot initialize the matrix. Lack of memory.\n");
		exit(EXIT_FAILURE);			
	}

	mat->ele_ty = va_arg(valist, int);

	if (DEBUG) {
		printf("*%d*\n", mat->ele_ty);
	}
	
	n--;

	for (int i = 0; i < n; i++) {
		mat->dim_szs[i] = va_arg(valist, int);
	}

	for (int i = 0; i < n; i++) {
		sum += mat->dim_szs[i];
		if (mat->dim_szs[i] <= 0) {
			printf("Cannot initialize the matrix. Some dimension size <= 0.\n");
			exit(EXIT_FAILURE);			
		}
		total *= mat->dim_szs[i];
	}
	mat->length = total;
	mat->start = (void*) malloc(mat->ele_ty * total);

	va_end(valist);

	if (DEBUG) {
		printf("Matrix initialized!");
		print_matrix(mat);
	}
}

void init_mat_value(struct Mat* mat, int n, ...) {
	if (n - 1 <= 0) {
		printf("Cannot initialize the matrix. The dimension <= 0.\n");
		exit(EXIT_FAILURE);
	}

	int sum = 0;
	int total = 1;

	va_list valist;
	va_start(valist, n);

	mat->nr_dim = n - 1;

	if (DEBUG) {
		printf("dim=%d\n",mat->nr_dim);
	}
	
	mat->dim_szs = (int*) malloc(sizeof(int) * mat->nr_dim);
	if (mat->dim_szs == NULL) {
		printf("Cannot initialize the matrix. Lack of memory.\n");
		exit(EXIT_FAILURE);			
	}

	mat->ele_ty = va_arg(valist, int);

	if (DEBUG) {
		printf("*%d*\n", mat->ele_ty);
	}
	
	n--;

	for (int i = 0; i < n; i++) {
		mat->dim_szs[i] = va_arg(valist, int);
	}

	for (int i = 0; i < n; i++) {
		sum += mat->dim_szs[i];
		if (mat->dim_szs[i] <= 0) {
			printf("Cannot initialize the matrix. Some dimension size <= 0.\n");
			exit(EXIT_FAILURE);			
		}
		total *= mat->dim_szs[i];
	}
	mat->length = total;
	mat->start = (void*) malloc(mat->ele_ty * total);


	//n += total;
	va_start(valist, n);

	for (int i = 0; i < n+1; i++) {
		va_arg(valist, int);
	}

	if (mat->ele_ty == 1) {
		bool* temp = (bool*) malloc(sizeof(bool) * mat->length);
		for (int i = 0; i < total; i++) {
			temp[i] = va_arg(valist, int);
		}
		mat->start = (void* )temp;
	}

	if (mat->ele_ty == 4) {
		int* temp = (int*) malloc(sizeof(int) * mat->length);
		for (int i = 0; i < total; i++) {
			temp[i] = va_arg(valist, int);
		}
		mat->start = (void* )temp;
	}

	if (mat->ele_ty == 8) {
		double* temp = (double*) malloc(sizeof(double) * mat->length);
		for (int i = 0; i < total; i++) {
			temp[i] = va_arg(valist, double);
		}
		mat->start = (void* )temp;
	}

	va_end(valist);

	if (DEBUG) {
		printf("Matrix initialized!\n");
	}

}

void* get_index(struct Mat* in_mat, struct Mat* out_mat, int index) {
	if (index >= in_mat->dim_szs[0]) {
		printf("Index out of range. Get index %d from matrix has size of %d \n", 
			index, in_mat->dim_szs[0]);
		exit(EXIT_FAILURE);			
	}

	int nr_dim = in_mat->nr_dim;

	out_mat->nr_dim = nr_dim - 1;
	out_mat->ele_ty = in_mat->ele_ty;

	out_mat->dim_szs = (int*) malloc(sizeof(int) * (nr_dim - 1));
	if (out_mat->dim_szs == NULL) {
		printf("Cannot initialize the get_index temp matrix. Lack of memory.\n");
		exit(EXIT_FAILURE);			
	}
	for (int i = 0; i < nr_dim - 1; i++) {
		out_mat->dim_szs[i] = in_mat->dim_szs[i + 1];
	}

	out_mat->length = in_mat->length / in_mat->dim_szs[0];

	out_mat->start = in_mat->start + index * out_mat->length * out_mat->ele_ty;

	return (out_mat->start);
}


void get_slice(struct Mat* in_mat, struct Mat* out_mat, int start, int end) {
	end = (end == -1) ? in_mat->dim_szs[0] : end;

	if (start < 0) {
		printf("Index out of range. Get slice with start = %d < 0\n", start);
		exit(EXIT_FAILURE);
	}

	if (end > in_mat->dim_szs[0]) {
		printf("Index out of range. Get slice with end = %d > %d\n", end, in_mat->dim_szs[0]);
		exit(EXIT_FAILURE);
	}

	int nr_dim = in_mat->nr_dim;

	out_mat->nr_dim = nr_dim;
	out_mat->ele_ty = in_mat->ele_ty;

	out_mat->dim_szs = (int*) malloc(sizeof(int) * nr_dim);
	if (out_mat->dim_szs == NULL) {
		printf("Cannot initialize the get_slice temp matrix. Lack of memory.\n");
		exit(EXIT_FAILURE);			
	}
	for (int i = 0; i < nr_dim; i++) {
		out_mat->dim_szs[i] = in_mat->dim_szs[i];
	}
	out_mat->dim_szs[0] = end - start;

	out_mat->length = (in_mat->length / in_mat->dim_szs[0]) * out_mat->dim_szs[0];

	out_mat->start = in_mat->start + start * (out_mat->length / out_mat->dim_szs[0]) * out_mat->ele_ty;

	if (DEBUG) {
		print_matrix(in_mat);
		print_matrix(out_mat);
	}
}

void get_dim(struct Mat* in_mat, struct Mat* out_mat) {
	out_mat->nr_dim = 1;
	out_mat->ele_ty = sizeof(int);

	out_mat->dim_szs = (int*) malloc(sizeof(int));
	if (out_mat->dim_szs == NULL) {
		printf("Cannot initialize the get_dim temp matrix. Lack of memory.\n");
		exit(EXIT_FAILURE);			
	}
	out_mat->dim_szs[0] = in_mat->nr_dim;

	int* temp = (int*) malloc(sizeof(int) * in_mat->nr_dim);
	if (temp == NULL) {
		printf("Cannot initialize the get_dim temp matrix. Lack of memory.\n");
		exit(EXIT_FAILURE);			
	}
	for (int i = 0; i < in_mat->nr_dim; i++) {
		temp[i] = in_mat->dim_szs[i];
	}

	out_mat->start = (void*) temp;
	out_mat->length = in_mat->nr_dim;
}

struct Mat* matrix_operation(struct Mat* mat1, struct Mat* mat2, int op) {
	struct Mat* mat3 = (struct Mat*) malloc(sizeof(struct Mat));

	mat3->nr_dim = mat1->nr_dim;
	mat3->dim_szs = mat1->dim_szs;
	mat3->length = mat1->length;
	mat3->ele_ty = mat1->ele_ty;
	if (mat3->ele_ty == 4) {
		int* temp = (int*) malloc(sizeof(int) * mat3->length);
		int* temp1 = mat1->start;
		int* temp2 = mat2->start;
		for (int i = 0; i < mat1->length; i++) {
			switch(op) {
				case 1:
					temp[i] = temp1[i] + temp2[i];
					break;
				case 2:
					temp[i] = temp1[i] - temp2[i];
					break;
				case 3:
					temp[i] = temp1[i] * temp2[i];
					break;
				case 4:
					temp[i] = temp1[i] / temp2[i];
					break;
				default:
					temp[i] = temp1[i] % temp2[i];

			}
		}
		mat3->start = (void*) temp;

	}

	if (mat3->ele_ty == 8) {
		double* temp = (double*) malloc(sizeof(double) * mat3->length);
		double* temp1 = mat1->start;
		double* temp2 = mat2->start;
		for (int i = 0; i < mat1->length; i++) {
			switch(op) {
				case 1:
					temp[i] = temp1[i] + temp2[i];
					break;
				case 2:
					temp[i] = temp1[i] - temp2[i];
					break;
				case 3:
					temp[i] = temp1[i] * temp2[i];
					break;
				case 4:
					temp[i] = temp1[i] / temp2[i];
					break;
				default:
					break;

			}
		}
		mat3->start = (void*) temp;
	}

	if (mat3->ele_ty == 1) {
		bool* temp = (bool*) malloc(sizeof(bool) * mat3->length);
		bool* temp1 = mat1->start;
		bool* temp2 = mat2->start;
		for (int i = 0; i < mat1->length; i++) {
			switch(op) {
				case 6:
					temp[i] = temp1[i] && temp2[i];
					break;
				case 7:
					temp[i] = temp1[i] || temp2[i];
					break;
				default:
					break;

			}
		}
		mat3->start = (void*) temp;
	}

	//mat3->start = (int*) malloc(sizeof(int) * mat1->length);
	//mat3->start = (void*) temp;

	return mat3;
}

struct Mat* matrix_int_operation(struct Mat* mat1, int v, int op) {
	struct Mat* mat3 = (struct Mat*) malloc(sizeof(struct Mat));

	mat3->nr_dim = mat1->nr_dim;
	mat3->dim_szs = mat1->dim_szs;
	mat3->length = mat1->length;
	mat3->ele_ty = mat1->ele_ty;
	int* temp = (int*) malloc(sizeof(int) * mat3->length);

	for (int i = 0; i < mat1->length; i++) {
		switch(op) {
			case 1:
				temp[i] = *((int *) (mat1->start + sizeof(int) * i)) + v;
				break;
			case 2:
				temp[i] = *((int *) (mat1->start + sizeof(int) * i)) - v;
				break;
			case 3:
				temp[i] = *((int *) (mat1->start + sizeof(int) * i)) * v;
				break;
			case 4:
				temp[i] = *((int *) (mat1->start + sizeof(int) * i)) / v;
				break;
			default:
				temp[i] = *((int *) (mat1->start + sizeof(int) * i)) % v;

		}
	}

	mat3->start = (void*) temp;

	return mat3;
}

struct Mat* matrix_bool_operation(struct Mat* mat1, bool v, int op) {
	struct Mat* mat3 = (struct Mat*) malloc(sizeof(struct Mat));

	mat3->nr_dim = mat1->nr_dim;
	mat3->dim_szs = mat1->dim_szs;
	mat3->length = mat1->length;
	mat3->ele_ty = mat1->ele_ty;
	bool* temp = (bool*) malloc(sizeof(bool) * mat3->length);

	for (int i = 0; i < mat1->length; i++) {
		switch(op) {
			case 1:
				temp[i] = *((bool *) (mat1->start + sizeof(bool) * i)) && v;
				break;
			case 2:
				temp[i] = *((bool *) (mat1->start + sizeof(bool) * i)) || v;
				break;
			default:
				temp[i] = *((bool *) (mat1->start + sizeof(bool) * i)) % v;

		}
	}

	mat3->start = (void*) temp;

	return mat3;
}

struct Mat* matrix_float_operation(struct Mat* mat1, double v, int op) {
	struct Mat* mat3 = (struct Mat*) malloc(sizeof(struct Mat));

	mat3->nr_dim = mat1->nr_dim;
	mat3->dim_szs = mat1->dim_szs;
	mat3->length = mat1->length;
	mat3->ele_ty = mat1->ele_ty;
	double* temp = (double*) malloc(sizeof(double) * mat3->length);

	for (int i = 0; i < mat1->length; i++) {
		switch(op) {
			case 1:
				temp[i] = *((double *) (mat1->start + sizeof(double) * i)) + v;
				break;
			case 2:
				temp[i] = *((double *) (mat1->start + sizeof(double) * i)) - v;
				break;
			case 3:
				temp[i] = *((double *) (mat1->start + sizeof(double) * i)) * v;
				break;
			case 4:
				temp[i] = *((double *) (mat1->start + sizeof(double) * i)) / v;
				break;
			default:
				break;

		}
	}

	mat3->start = (void*) temp;

	return mat3;
}

// int main() {
// 	struct Mat* mat1 = (struct Mat*) malloc(sizeof(struct Mat));
// 	// struct Mat* mat2 = (struct Mat*) malloc(sizeof(struct Mat));
// 	init_mat_value(mat1, 4, 4, 2, 3, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
// 	print_matrix(mat1);
// 	return 0;
// }


import os
from os import listdir
from os.path import isfile, join

total = 0
passed = 0
failed = 0

os.system("make clean")
os.system("make all")

filenames = [f for f in listdir("tests") if isfile(join("tests", f))]
filenames.sort()

failed_tests = []

for filename in filenames:
    total = total + 1
    print("======== Running: {filename} ========".format(filename=filename))

    name = filename.split('.')[0]
    flag = name.split('_')[-1]

    print("flag: {flag}".format(flag=flag))
    command = "./mpp.native tests/{filename}".format(filename=filename)

    try:
        out = os.system(command)
        # print("out = {out}".format(out=out))

        # if the test should fail, throws an exception
        if out == 0:
            os.system("./a.out")
            passed = passed + 1
            print("this test passes")
        else:
            raise Exception("Fail parsing source program!")
    except:
        print("caught exception!")
        if flag == "pass":
            failed = failed + 1
            failed_tests.append(filename)
            print("this test fails")
        else:
            passed = passed + 1
            print("this test passes")
    print("")

print("=====================================")
print("============== Results ==============")
rate = round(passed / total * 100, 3)
print("{total} tests executed, {passed} passed, {failed} failed. Pass rate = {rate}%\n"
      .format(total=total, passed=passed, failed=failed, rate=rate))

if failed > 0:
    print("failed tests: \n")
    failed_tests.sort()
    for test in failed_tests:
        print(test)
    print("\n")

os.system("make clean")

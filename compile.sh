#!/usr/bin/env bash
ocamllex scanner.mll # create scanner.ml
ocamlc -c ast.ml # compile AST types
ocamlyacc parser.mly # create parser.ml and parser.mli
# ocamlc -c sast.ml # compoile ASAT types
ocamlc -c parser.mli # compile parser types
ocamlc -c scanner.ml # compile the scanner
ocamlc -c parser.ml # compile the parser
# ocamlc -c semant.ml # compile the semat check
ocamlc -c test1.ml # compile the interpreter
ocamlc -o test1 ast.cmo parser.cmo scanner.cmo test1.cmo
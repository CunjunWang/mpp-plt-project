// a multi-dimensional in matrix
#include <stdbool.h>
struct Mat {
	int nr_dim;
	int ele_ty;
	int* dim_szs;
	void* start; // point to the first int element
	int length;
};
void print_int_mat(int* start,int dim,int*dim_szs,int length);
void print_double_mat(double* start,int dim,int*dim_szs,int length);
void print_bool_mat(bool* start,int dim,int*dim_szs,int length);
void print_matrix(struct Mat* mat);
void init_mat(struct Mat* mat, int n, ...);
int print_int(int n);
void* get_index(struct Mat* in_mat, struct Mat* out_mat, int index);
void get_slice(struct Mat* in_mat, struct Mat* out_mat, int start, int end);
void get_dim(struct Mat* in_mat, struct Mat* out_mat);
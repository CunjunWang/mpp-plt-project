\documentclass{report}
\usepackage[utf8]{inputenc}
\usepackage[margin=0.875in]{geometry}
\usepackage{hyperref}
\hypersetup{
    linktoc=all
}
\usepackage{float}
\usepackage{tikz}
\usetikzlibrary{shapes, arrows}
\usetikzlibrary{positioning}

\setcounter{tocdepth}{3}
\setcounter{secnumdepth}{3}

\title{\textbf{M++ Final Report}}
\author{Yonghe Zhao - yz3687 \\ Jierui Liu - jl5490 \\ Junyu Chao - jc5289 \\  Yu Wang - yw3348 \\ Cunjun Wang - cw3199}
\date{May 2020}

\begin{document}

    \fontsize{12}{14}\selectfont

    \maketitle

    \tableofcontents

    \chapter{Introduction}
    \section{Motivation}
    With an unprecedented development and widely use of data analysis and machine learning techniques, analyzing and manipulating hundreds of thousands of byte of data is playing an indispensable role in our daily work, and the size and dimension of data to process is still growing rapidly. Matrix, a multi-dimensional array of scalars with one or more columns and one or more rows, is often the form of input data, and the manipulations of matrices is often essential to machine learning algorithms.
    \\
    \\ One mainstream tool for matrix manipulation is \verb|Matlab|, which is powerful but extremely expensive for those individuals and companies that have low budget. Other common languages are \verb|python| and \verb|julia|, but they also depend on importing official or third party libraries in order to work smoothly. Implementing these operations in other popular programming languages such as \verb|Java| and \verb|C++| can be much more complicated and painful. With this motivation, we want to design a language that natively support an easy and fast way to display and operate matrices.

    \section{Background}
    \verb|M++| is a versatile programming language that is mainly designed to facilitate matrix manipulations. The syntax and semantics of our language is a combination of \verb|C| and \verb|Go|. With the \verb|M++| language, the user can declare matrices using a simple and intuitive notation. We also provide powerful built-in functions to support common matrix operations.
    \\
    \\ In addition to the matrix type \verb|mat|, our language supports other primitive types include \verb|string|, \verb|int|, \verb|bool| and \verb|float|, together with basic operations on them.



    \chapter{Language Tutorial}
    \section{Setup}
    \subsection{Installation}
    To setup the environment, \verb|M++| requires a few dependencies to be installed. This section will describe the installation process of the \verb|M++| compiler on multiple operating systems.
    \\
    \\ For all environments, download and unzip the \verb|m++.zip| first. You will see a new folder \verb|mpp| with compiler source files. Set the current working directory in command line with \verb|cd mpp|.

    \subsubsection{MacOS 10.15}
    On MacOS, we assume you have installed \verb|homebrew| or other package management plugins.
    \\
    \\ You need to install \verb|opam| and \verb|LLVM| first:
    \begin{verbatim}
    brew install opam
    brew install llvm@9
    \end{verbatim}
    In folder \verb|mpp|, run commands
    \begin{verbatim}
    opam init
    opam install llvm
    eval 'opam config env'
    \end{verbatim}

    \subsubsection{Ubuntu 16.04}
    You need to install the \verb|opam| first:
    \begin{verbatim}
    add-apt-repository ppa:avsm/ppa
    apt-get update
    apt-get install ocaml opam
    \end{verbatim}
    And then install \verb|LLVM|:
    \begin{verbatim}
    opam init
    opam install llvm
    eval 'opam config env'
    \end{verbatim}


    \subsection{Build the Compiler}
    After installing all dependencies successfully, you can compile the \verb|M++| compiler by running \verb|make all| in the \verb|M++| root directory.

    \subsection{Run Sample Code}
    You can create a new file with any name, but end with a \verb|.mpp| suffix, for example, \verb|example.mpp|. Then you can write code int \verb|M++| and execute it with
    \begin{verbatim}
    ./run.sh example.mpp
    \end{verbatim}
    or with
    \begin{verbatim}
    sh run.sh example.mpp
    \end{verbatim}
    The comprehensive syntax tutorial is in a separate \verb|M++| language reference manual. You can take a try with example code provided in \hyperref[sec:example]{section 2.3}.

    \section{Basic Syntax}
    In this section we just briefly introduce the basic syntax of \verb|M++|. Users can refer to the Language Reference Manual for more details.
    \\
    \\ \verb|M++| languages syntax is a combination of \verb|C| and \verb|Go| syntax.
    \begin{itemize}
        \item[-] For each program, the program entry point must be defined as a \verb|main| function;
        \item[-] Every statement must end with a semicolon \verb|;| ;
        \item[-] Every function must have a primitive return type. When the function does not need a return value, users should use \verb|void| as the return type, and write \verb|return;| at the end of function body;
        \item[-] Users should include parameter type in the form of \verb|type parameter-name| when declaring a function, and multiple parameters shuold be separated by commas \verb|,|;
        \item[-] Users can declare a new variable with a \verb|:=| operand with or without mentioning its type in the front of the expression;
        \item[-] Functions are declared with \verb|func| keyword;
    \end{itemize}

    \section{Example Programs}
    \label{sec:example}
    \verb|M++| supports easy declaration and manipulation of matrices. In order to declare a matrix, you can either declare with \verb|mat| keyword, or with \verb|:=| operand. You can declare a matrix with specified entries, or just with type and dimensions, as shown below in the example:
    \begin{verbatim}
    int func main() {
            /*
            define a matrix variable with := operator;
            declare an integer matrix (all zeros) with dimensions 2 * 3
         */
        m := mat(int, 2, 3);

        /*
            define a matrix variable with mat constructor;
            entry type is int, dimension is 2;
            declare an integer matrix with elements;
            n is also a 2 * 3 matrix
         */
        mat<int, 2> n := [ [1, 2, 3],
            [4, 5, 6] ];

        /* assign value to some position of matrix m */
        m[1][1] = 4;

        /* add two matrices */
        res := m + n;

        /* access some entry and assert it to the expected value */
        assert(res[1][1] == 9);

        /* print the result */
        print_matrix(res);

        return 0;
    }
    \end{verbatim}
    \verb|M++| supports some built-in methods for the matrix data type, such as
    \begin{itemize}
        \item[-] \verb|get_dim(mat A)|: Get dimensions of input matrix \verb|A|.
        \item[-] \verb|print_matrix(mat A)|: Print the input matrix \verb|A|.
    \end{itemize}
    \verb|M++| also has some built-in methods to help users write demo and tests:
    \begin{itemize}
        \item[-] \verb|assert(bool expr)|: Assertion function.
        \item[-] \verb|assert_msg(string msg, bool expr)|: Assertion function with error message.
        \item[-] \verb|printf(string tpl, args...)|: Print the given template with arguments. The usage is the same as \verb|printf| in \verb|C|.
    \end{itemize}




    \chapter{Architectural Design}
    \section{Architecture Diagram}
    \label{sec:diagram}
    \textbf{Bold text} means a phase, \textit{italic text} means the outputs, \underline{underlined text} means tests.

    \tikzstyle{block} = [rectangle, draw, text width=6em, text centered, rounded corners, minimum height=4em]
    \tikzstyle{line} = [draw, -latex']

    \begin{figure}[H]
        \centering
        \begin{tikzpicture}[node distance = 2.2cm, auto]
            % Place nodes
            \node [block] (source) {M++ Source Program};
            \node [block, right=1.5cm of source] (scanner) {\textbf{Scanner}};
            \node [block, right=1.5cm of scanner] (scanT) {Match input with tokens};
            \node [block, right=1.5cm of scanT] (token) {\textit{Tokens}};
            \node [block, below of=scanner] (parser) {\textbf{Parser}};
            \node [block, right=1.5cm of parser] (parserT) {Group tokens into expressions};
            \node [block, right=1.5cm of parserT] (ast) {\textit{AST}};
            \node [block, text width=12em, below of=parserT] (test1) {\underline{Tests for parsed program}};
            \node [block, below=3cm of parser] (semantic) {\textbf{Semantic Check}};
            \node [block, right=1.5cm of semantic] (semantT) {Check types in AST};
            \node [block, right=1.5cm of semantT] (sast) {\textit{SAST}};
            \node [block, text width=12em, below of=semantT] (test2) {\underline{Tests for semantic check}};
            \node [block, below=3cm of semantic] (irgen) {\textbf{IR-generation}};
            \node [block, right=1.5cm of irgen] (irgenT) {Build \verb|LLVM| Modules};
            \node [block, right=1.5cm of irgenT] (inter) {\textit{Intermediate Code}};
            \node [block, below of=irgenT] (exec) {\textit{Executable File}};
            \node [block, text width=12em, right=1.5cm of exec] (test3) {\underline{Final Black Box Tests}};
            % Draw edges
            \path [line] (source) -- (scanner);
            \path [line] (scanner) -- (scanT);
            \path [line] (scanT) -- (token);
            \path [line] (token) -- (parser);
            \path [line] (parser) -- (parserT);
            \path [line] (parserT) -- (ast);
            \path [line] (ast) -- (test1);
            \path [line] (test1) -- (semantic);
            \path [line] (semantic) -- (semantT);
            \path [line] (semantT) -- (sast);
            \path [line] (sast) -- (test2);
            \path [line] (test2) -- (irgen);
            \path [line] (irgen) -- (irgenT);
            \path [line] (irgenT) -- (inter);
            \path [line] (inter) -- (exec);
            \path [line] (exec) -- (test3);
        \end{tikzpicture}
    \end{figure}

    \section{Component Interfaces}
    \subsection{Scanner}
    \textit{Implementation by Jierui Liu and Yonghe Zhao.}
    \\
    \\ The scanner is the first layer of the \verb|M++| compiler. It reads a source program input stream as input.
    \\
    \\ During this phase, the source program are split by the separators and white spaces. The characters are then grouped into tokens according to pre-defined token lists. Tokens include all of the reserved language keywords, variable names, function names, string, integer, matrix representation, braces or brackets as well as any other literals.
    \\
    \\  The output of this phase is a token stream.

    \subsection{Parser}
    \textit{Implementation by Jierui Liu, Yonghe Zhao, Junyu Chao and Cunjun Wang.}
    \\
    \\ The parser takes the token stream generated by lexer as its input. It is responsible for using these input tokens to construct an output \verb|AST|. Our implementation follows that of the \verb|micro-C| compiler.
    \\
    \\ The \verb|M++| program consists of an optional series of include statements, an optional series of variable global variable declarations and a series of function declarations, in which the \verb|main| function declaration is mandatory.

    \subsection{Semantic Checking}
    \textit{Implementation by all five members.}
    \\
    \\ The semantic checker traverses the \verb|AST| generated by parser. It verifies the semantics of the program and performs the type check, including but not limited to variable type and function return type. It also builds and maintains a symbol table to check the lexical scope and control the code blocks.
    \\
    \\ The output of this phase is an \verb|SAST|.

    \subsection{Code Generation}
    \textit{Implementation by Yonghe Zhao and Jierui Liu.}
    \\
    \\ The code generator takes the \verb|SAST| and returns an \verb|LLVM| module, in which the expressions are evaluated and statements are executed. It is also responsible for linking to \verb|C| library to include built-in functions.
    \\
    \\ The output of this phase is an executable file which takes an \verb|M++| source program, and then compile and execute it.

    \subsection{Built-in Functions}
    \textit{Implementation by Jierui Liu, Yonghe Zhao and Yu Wang.}
    \\
    \\ Provides some built-in functions implemented in \verb|C| language for users to print, assert and manipulate matrices.

    \subsection{Test Suites}
    \textit{Implementation by Cunjun Wang and Yonghe Zhao.}
    \\
    \\ Design and implemented a bunch of test suites written in \verb|M++| to make the \verb|M++| compiler robust.



    \chapter{Test Plan}
    \section{Test Plans and Automation Used}
    Our initial test plan was writing unit tests for each compiler phase after its implementation and before starting the next phase, in order to make sure that higher layer in the architecture was built on a robust and bug-free lower layer.
    \\
    \\ However, when writing tests we found that the output of \verb|OCaml| built-in lexing library could only be read by \verb|OCaml| built-in parser library, so it was impossible to test them separately. We then decided to follow the test scheme in \verb|micro-C|.

    \subsection{Command Line Test Program}
    We wrote two test file called \verb|test1.ml| and \verb|test2.ml| that takes command line inputs and evaluate. We followed the filename convention in the \verb|micro-C| compiler.
    \\
    \\ We used \verb|OCaml| built-in lexer and parser library. During the implementation phase, it was common that we got an exception thrown from the library but we had no idea which line of our source program was wrong. With these command line tests, we were able to provide \verb|M++| source program line by line to the parser, and got exception immediately at the line wasn't recognized by our parser. These tests helped us to locate the bug efficiently.

    \subsection{Unit Test Suites}
    After the implementation phase, we planned to design a bunch of unit tests and execute them with one command. \verb|test_parser.ml| and \verb|test_semant.ml| include test cases that take a block of \verb|M++| source program as input, and pass the program into parser and semantic check respectively.
    \\
    \\ We designed test suites as follow:
    \begin{itemize}
        \item[-] The first set of tests came after the implementation of AST. The goal for these tests was to split the input source program into correct tokens, and parse them into a correct AST.
        \item[-] After the implementation of SAST and semantic and type check, we implemented the second test suite. The goals include checking variable assignment types and function return types, throwing exceptions when types are mismatching, and identifying function and code blocks for a working scope.
    \end{itemize}

    \subsection{Final Black Box Tests}
    After the \verb|LLVM| module was integrated to our compiler and the built-in functions are implemented, we designed comprehensive test suites includes a bunch of files with suffix \verb|.mpp|. They are all in the \verb|tests| directory, and the naming conventions for the tests are
    \begin{verbatim}
    {test-number}_{test-case-description}_{expected-pass/fail-flag}.mpp
    \end{verbatim}
    We also wrote a \verb|python| script called \verb|run_tests.py| to read these tests files, compile and execute, and compare the result with the expected flag. We also counted the total executed tests, passed tests and failed tests, and computed a pass rate. We used this script to conveniently do regression test again and again whenever we modified our implementation.
    \\
    \\ When some bugs or errors are found, the test raise an issue in our git repository, and the developer who is responsible for the module starts the debug process. When the bug is fixed, the tester run the test suites again the ensure a pass, and close the issue.
    \\
    \\ You can also see the test layers in the architecture diagram in \hyperref[sec:diagram]{section 3.1}.

    \section{Sample Programs and Generated Code}
    \subsection{Declaring Matrices}
    This is a simple test program designed to test declaring and a matrix in \verb|M++|:
    \begin{verbatim}
    int func main() {
            a := [ [1, 2], [3, 4] ];
        return 0;
    }
    \end{verbatim}
    Expected output is:
    \begin{verbatim}
    a = [ [0, 0],
            [0, 0] ]
    \end{verbatim}
    Here is the generated \verb|LLVM| module:
    \begin{verbatim}
    ; ModuleID = 'Mpp'
    source_filename = "Mpp"

    %Mat = type { i32, i32, i32*, i8*, i32 }

    declare i32 @printf(i8*, ...)

    declare void @print_matrix(%Mat*)

    declare void @assert(i1)

    declare void @assert_msg(i8*, i1)

    declare void @init_mat(%Mat*, i32, ...)

    declare void @init_mat_value(%Mat*, i32, ...)

    declare void @get_dim(%Mat*, %Mat*)

    declare i8* @get_index(%Mat*, %Mat*, i32)

    declare void @get_slice(%Mat*, %Mat*, i32, i32)

    declare %Mat* @matrix_operation(%Mat*, %Mat*, i32)

    declare %Mat* @matrix_int_operation(%Mat*, i32, i32)

    declare %Mat* @matrix_float_operation(%Mat*, double, i32)

    declare %Mat* @matrix_bool_operation(%Mat*, i1, i32)

    define i32 @main() {
            entry:
        %mat = alloca %Mat
        call void (%Mat*, i32, ...) @init_mat_value(%Mat* %mat, i32 3,
            i32 4, i32 2, i32 2, i32 1, i32 2, i32 3, i32 4)
    %new_mat = load %Mat, %Mat* %mat
    %a = alloca %Mat
            store %Mat %new_mat, %Mat* %a
        ret i32 0
    }

    \end{verbatim}

    \subsection{Functions and Arithmetic Operation}
    This is a simple test program designed to test functions and arithmetic operations in \verb|M++|:
    \begin{verbatim}
    int func abs(int a) {
            if (a >= 0) {
            return a;
    } else {
            return -a;
    }
    }

    int func main() {
            int a = -5;
        int b = 8;
        res := abs(a) + b;
        return 0;
    }
    \end{verbatim}
    Expected output is:
    \begin{verbatim}
    13
    \end{verbatim}
    Here is the generated \verb|LLVM| module:
    \begin{verbatim}
     ModuleID = 'Mpp'
    source_filename = "Mpp"

    %Mat = type { i32, i32, i32*, i8*, i32 }

    declare i32 @printf(i8*, ...)

     declare void @print_matrix(%Mat*)

     declare void @assert(i1)

     declare void @assert_msg(i8*, i1)

     declare void @init_mat(%Mat*, i32, ...)

     declare void @init_mat_value(%Mat*, i32, ...)

     declare void @get_dim(%Mat*, %Mat*)

     declare i8* @get_index(%Mat*, %Mat*, i32)

     declare void @get_slice(%Mat*, %Mat*, i32, i32)

     declare %Mat* @matrix_operation(%Mat*, %Mat*, i32)

    declare %Mat* @matrix_int_operation(%Mat*, i32, i32)

    declare %Mat* @matrix_float_operation(%Mat*, double, i32)

    declare %Mat* @matrix_bool_operation(%Mat*, i1, i32)

    define i32 @main() {
             entry:
      %a = alloca i32
      store i32 -5, i32* %a
      %b = alloca i32
      store i32 8, i32* %b
      %a1 = load i32, i32* %a
      %abs_result = call i32 @abs(i32 %a1)
      %b2 = load i32, i32* %b
      %tmp = add i32 %abs_result, %b2
      %res = alloca i32
      store i32 %tmp, i32* %res
      ret i32 0
     }

     define i32 @abs(i32 %a) {
    entry:
      %a1 = alloca i32
      store i32 %a, i32* %a1
      %a2 = load i32, i32* %a1
      %tmp = icmp sge i32 %a2, 0
      br i1 %tmp, label %then, label %else

    then:                                             ; preds = %entry
      %a3 = load i32, i32* %a1
      ret i32 %a3

    else:                                             ; preds = %entry
      %a4 = load i32, i32* %a1
      %tmp5 = sub i32 0, %a4
      ret i32 %tmp5

    if_end:                                           ; No predecessors!
      ret i32 0
}
\end{verbatim}


\chapter{Summary}
\section{Project Timeline}
\begin{table}[H]
\begin{tabular}{cl}
\hline
\textbf{Date} & \multicolumn{1}{c}{\textbf{Milestone}}  \\ \hline
February 24   & First meeting. Decided what language to design and the basic plan.  \\ \hline
March 2       & Git Repository generated, started our implementation. \\ \hline
March 19      & Scanner and Parser completed.  \\ \hline
March 23      & \verb|AST| and \verb|SAST| completed. \\ \hline
March 29      & Added some tests for previous parts. Started to implement semantic checking. \\ \hline
April 13      & \begin{tabular}[c]{@{}l@{}}\verb|AST| print functions added and tests for Scanner, Parser, \verb|AST| completed.\\ Started to implement IR Generation.\end{tabular}  \\ \hline
April 17      & \begin{tabular}[c]{@{}l@{}}\verb|SAST| print functions added and tests for semantic part completed. \\ Started to integrate \verb|LLVM|.\end{tabular} \\ \hline
April 25      & \begin{tabular}[c]{@{}l@{}}Run the example.mpp successfully and started to add built-in functions. \\ Regression tests for previous part passed.\end{tabular} \\ \hline
May 5         & \verb|M++| basic functions completed. Continued to add more built-in functions. \\ \hline
May 13        & Final Report completed. Project finished. \\ \hline
\end{tabular}
\end{table}

\section{Roles and Responsibilities}
% Manager: Timely completion of deliverables
% Language Guru: Language design
% System Architect: Compiler architecture, development environment
% Tester: Test plan, test suites
\textbf{Yonghe Zhao (Manager):} Specify the language design, consider what to do next and distribute the work load to the team members.  Implement the main framework of the syntax analysis, semantic analysis and intermediate code generation. Write the built-in functions for the test framework.
\\
\\ \textbf{Jierui Liu (Language Guru):} Write scanner and initial version of \verb|AST| and \verb|SAST| file. Implement pointer and matrix operations in \verb|LLVM| and corresponding \verb|C| functions. Also implement matrix initialization with array-like structure, including all corresponding \verb|C| functions.
\\
\\ \textbf{Junyu Chao (Language Guru):} Define the basic grammar of matrix and higher order function in \verb|Parser| and \verb|AST|.
\\
\\ \textbf{Yu Wang (System Architect):} Write built-in function and help do some tests.
\\
\\ \textbf{Cunjun Wang (Tester):} Write print functions in \verb|AST| and \verb|SAST| to facilitate debug process. Design and implemented unit test suites in \verb|/tests| folder, and test scripts \verb|run_tests.py|. Write final report and language reference manual.


\section{Lessons Learned and Future Advice}
\subsection{Yonghe Zhao}
\subsubsection{Lessons Learned}
When I was a junior, the homework of the Compiling Principle course was almost completed by an extremely intelligent student, who is also my friend, in my group. At that time, my programming skill was very poor. I wanted to help him but I could not. This is also a pity of my college life. Fortunately, in this course, I finally participated in the implementation of a compiler as a core member. By doing this project, I get a clearer understanding of the compilation process of the program, from lexical analysis to code generation. At the same time, I also have a deeper understanding of the existing mainstream programming language design. When designing \verb|M++| language, sometimes I think hard about how to realize a feature, but in the end, I often find inspiration in the excellent design of these mainstream languages. Many times, I try to design some more concise usage for \verb|M++|, but I find it very difficult to implement it. You almost cannot remove anything on the top of those mainstream languages design. I have to admit that the design of those languages is the best. In addition, I actually experienced team project development. During this period, I learned to use GitLab skillfully, including the branch management that I had been confused about before. This is of great help to my future career.
\\
\\
The cooperation with the team members also made me gain a lot. The test system led by Wang Cunjun has greatly improved the development efficiency. He wrote an automated test script that can quickly check the correctness of hundreds of test cases. This allows us to find bugs easily and quickly. After I fix a bug, I can execute the script again to confirm whether the bug has been solved and whether a new bug has been introduced. I also wrote two built-in functions \verb|assert| and \verb|assert_msg|, which enables our testing framework to check the correctness of the program on the numerical level. In addition, he will open issue on GitLab when finding new bugs. Issue is a function I haven't used before, which makes the process of bug fixing more like playing a game with several levels.
\\
\\
Trust and communication among team members is very important. In this project, I sometimes feel confident that many features can only be implemented by me, and I am the main driver of this project. So, in the early and mid-term, a lot of new features are implemented by me alone. Although my ability is strong, in some time periods, I am the only one to promote this project. This is obviously inefficient. In the end, my workload became very large, and the team members' understanding of my work was only superficial. When I encountered problems, the team members could not provide effective help. If I can redo this project, I will regularly urge the team members to read the source code of the current stage, fully trust the team members' ability, and reasonably assign work to them. If so, I believe we can achieve much more than now.
\\
\\
Of course, a clear division of tasks is based on good design and planning. In fact, the project did not do a good job in the early design and planning. This leads to the confusion of requirements in the process of code writing, and it is difficult to divide tasks. This is a profound lesson, but also my valuable experience.

\subsubsection{Future Advice}
I advice future groups to keep building \verb|M++| on top of our work. The garbage collection system is the most important thing that needed to be added in the future. Now, \verb|M++| will cause the memory leak when doing the matrix arithmetic. Also, the memory layout design of the matrix should be modified. Now, we cannot define a global matrix using \verb|M++|, because the matrix can only be initialized in the run time by calling several \verb|malloc|.

\subsection{Jierui Liu}
\subsubsection{Lessons Learned}
We did not start this project early. Our repository was created at the beginning of the spring Break. However, the sudden disease breaks up our whole plan and makes it impossible for us to hold any face to face meetings. Under such a severe circumstance, I do appreciate all my teammates to contribute to this project consistently. Together we overcome difficulties and obstacles that we have never encountered before and managed to accomplish a decent job. I learned how to handle team projects remotely and continuously make contributions and collaborate. Moreover, I also learned a precious lesson from my teammates about how to separate the large project into small pieces and mini-goals and then finish them one by one. I believe my personal time management skill is significantly improved via doing this project.

\subsection{Junyu Chao}
\subsubsection{Lessons Learned}
My takeaway can be divided into two parts. The project per se is a big challenge. It is true that we use various kinds of programming languages every day, but building a language from scratch is a totally different story. This project provides me with a thorough view of the very detailed part of a language. Some basic features, for example, type inference which we take for granted, can be hard to implement. What’s more, designing holistically is very essential. Some problems won’t become visible until several steps later, but it is already too hard to correct. I reckon only after designing and implementing a language by ourselves can we gain a insight of why we need so many languages and why each programming language has its own pros and cons, and this project can be such a good practice.
\\
\\
Besides the technical part, I learned the importance of teamwork. Due to the outbreak of Covid-19, the grade is no longer a motivation for us to keep working hard. It is possible that everyone is nonchalant about the project since they may still pass. However, my teammates are still serious about the project. They did perfect work and it is them who inspired me to earnestly contribute to \verb|M++|. So, I want to sincerely express my gratitude to all my teammates.
\subsubsection{Future Advice}
In terms of our language, we can add more powerful built-in functions and libraries. Optimizations can be done to accelerate the calculation of matrix as well.

\subsection{Yu Wang}
\subsubsection{Lessons Learned}
I have to admit that I took very few works in this project. I think there are 2 reasons. Firstly I am not active enough in the beginning. In the spring break, my teammates are working hard, but I am doing other things. So they have taken most of the parts when I realized I have to do something. Secondly the COVID-19 cut us off, so we were not able to sit down work together. Fortunately, my teammates work really hard so that we could finish this project very well, so I really thank my teammates from the bottom of my heart. Without their hardworking, our project could not be so successful.
\\
\\
Since I didn't participate in most parts, I didn't learn ocaml and PLT very well. I only have some basic understanding, but I don't learn much on design ideas, difficulty points and language details. That is really a regret. Next time I need to become more active for teamwork.

\subsubsection{Future Advice}
More built-in functions need to be developed to support more functions.

\subsection{Cunjun Wang}
\subsubsection{Lessons Learned}
I always believe that communication and planning are not only critical for an successful academic project but also important in work environment, and project experience for this project illustrates this opinion. Under the extraordinary circumstance caused by the COVID-19 pandemic, we were unable to hold regular in-person meetings and help each other to solve problems, and we could not get much help from TAs via virtual office hours, neither. We held telephone meetings every two weeks, but I think this less effective than a face to face talk. It was also much difficult to track the project progress and to keep the plan as scheduled since we didn't know what other teammates are doing. The foremost thing I learned from this project is that, communication and keeping the plan is essential for a team project.
\\
\\ As a tester in the team, the second thing I learned from this project is that, I need to be honest and transparent to my team. I should post questions and issues to my teammates when I have confusions in code review, or when I find bugs that I was unable to handle. I fixed some bugs when implementing pretty print functions for parser and semantic check, and I should told my team mates in order to avoid conflicts or duplicate works.

\subsubsection{Future Advice}
My first advice is to plan the project thoroughly before implementation. Work split should also be tailored well in order to involve all team members in the project as evenly as possible.
\\
\\ Secondly, for any future project, I think the earlier the team starts, the better the final achievement. For this project, the architecture layer has already been separated for us (scanner, parser, semantic check and code-generation), it is better to take some time to involve a design phase prior to coding in future project, and choose to implement features that build on each other.




\end{document}

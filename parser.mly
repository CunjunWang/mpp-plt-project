%{ 
  
open Ast


let get_mat_szs (t: typ): int list =
  match t with
  | Matrix(ty, li) -> li
  | _ -> raise (Failure "not a matrix")

let get_mat_ele_typ (t: typ): typ = 
  match t with
  | Matrix(ty, li) -> ty
  | _ -> raise (Failure "not a matrix")

let rec get_list_of_len (length: int): int list = 
  match length with
  | 0 -> []
  | a -> -1::(get_list_of_len (length - 1))


let type_to_enum (t: typ): int = 
  match t with
  | Int -> 4
  | Float -> 8
  | Bool -> 1
  | _ as ty -> raise (Failure ("Do not support " ^ (string_of_typ ty) ^ " matrix."))


let get_mat_type (mat_def: expr): typ =
	let a: int list ref=ref [] (*and ttype=ref Pending*) in
	(*let replace pos l = List.mapi (fun i x->if i=pos then x+1 else x) l in *)
	let rec dfs (level: int) (matrix: expr): unit =
	( match matrix with
	Mat(m)->if (List.length !a) = level then a:=(!a)@[List.length(m)] 
          else 
        if List.length m !=List.nth !a level then raise (Failure "Matrix dimension error!");let _=List.map (dfs (level+1)) m in ()
	(*| IntLit(a)-> if !ttype != Int then 
									if !ttype=Pending 
									then ttype:=Int 
									else raise (Failure "Inconsistent matrix type")					
	| BoolLit(a)-> if !ttype != Bool then 
									if !ttype=Pending 
									then ttype:=Bool
									else raise (Failure "Inconsistent matrix type")
	| FloatLit(a)-> if !ttype != Float then 
									if !ttype=Pending 
									then ttype:=Float
									else raise (Failure "Inconsistent matrix type")
	| StrLit(a)-> if !ttype != String then 
									if !ttype=Pending 
									then ttype:=String
									else raise (Failure "Inconsistent matrix type") *)
								
	| _ -> ())
	in
	dfs 0 mat_def;
	Matrix(Pending,!a) 

%}

%token SEMI LPAREN RPAREN LBRACE RBRACE LBRACK RBRACK COMMA COLON
%token PLUS MINUS TIMES DIVIDE ASSIGN INIT NOT MOD
%token EQ NEQ LT LEQ GT GEQ TRUE FALSE AND OR
%token RETURN IF ELSE ELSEIF FOR WHILE
%token BREAK CONTINUE FUNC INCLUDE
%token TMAT TINT TFLOAT TBOOL TSTRING TVOID

%token <int> ILIT
%token <bool> BLIT
%token <float> FLIT
%token <string> SLIT
%token <string> ID

%token EOF

%start program
%type <Ast.program> program

%right ASSIGN INIT
%left OR
%left AND
%left EQ NEQ
%left LT GT LEQ GEQ
%left PLUS MINUS
%left TIMES DIVIDE MOD
%right NOT NEG

%nonassoc ELSEIF
%nonassoc ELSE

%%

program:
    includ_opt decl_lists EOF { $1, fst $2, snd $2 }

/*
include files, must be at the first part of the program 
*/
includ_opt:
   /* nothing */ { [] }
  | includ_list   { $1 }

includ_list:
    includ { [$1] }
  | includ includ_list  { $1 :: $2 }

includ:
  | INCLUDE SLIT SEMI { FileIncl($2) }

decl_lists:
    { ([], []) }
  | gvdecl SEMI decl_lists { (($1 :: fst $3), snd $3) }
  | fdecl decl_lists { (fst $2, ($1 :: snd $2)) }

/* global variables declaration */
gvdecl: vdecl { $1 }

/* local variables declaration */
lvdecl: vdecl { $1 }

typ_opt:
   /*Nothing*/ {[]}
  | typ_list {$1}

typ_list:
    all_typ {[$1]}
  | all_typ COMMA typ_list  {$1::$3}

/*Same as rtyp, except for void*/
all_typ:
    typ {$1}
  | func_typ {$1}
  | mat_typ {$1}

typ: 
    TBOOL { Bool }
  | TINT { Int }
  | TFLOAT { Float }
  | TSTRING { String }



func_typ:
    FUNC rtyp LPAREN typ_opt RPAREN {Fun(None, $2, $4)} 


mat_typ:
  TMAT LT typ COMMA ILIT GT { Matrix($3, get_list_of_len $5) }

/* variables declaration */
vdecl:
    ID INIT expr { Init(Pending, $1, $3) }
  /* | ID INIT mat_def_expr { Init(get_type mat_def_expr, $1, $3) } */
  | ID INIT mat_def_expr { Init(get_mat_type $3, $1, $3) }
  /* TODO: can support only int list */
  /* int: 4, bool: 1, float: 8 */
  /* m := mat(int, 2, 2, 3) */
  | ID INIT TMAT LPAREN typ COMMA actual_list RPAREN 
    { if $5 = String then raise ( Failure "Do not support string matrix.") 
     else Init(Matrix($5, get_list_of_len (List.length $7)), $1, Call("init_mat", IntLit(type_to_enum $5)::$7)) } 
  | typ ID ASSIGN expr { Init($1, $2, $4) }
  | mat_typ ID ASSIGN mat_def_expr 
    { Init((let szs = get_mat_szs $1 and mat_ty = get_mat_ele_typ $1 in 
      let szs' = (get_mat_szs (get_mat_type $4)) in
      if List.length szs = List.length szs' then 
      Matrix(mat_ty, szs') else raise(Failure "Dimension not match.")), $2, $4) }
  /* TODO: can support only int list */
  | mat_typ ID ASSIGN TMAT LPAREN typ COMMA actual_list RPAREN 
    { Init((let szs = get_mat_szs $1 and mat_ty = get_mat_ele_typ $1 in 
      (match mat_ty with
      | _ when $6 = String -> raise ( Failure "Do not support string matrix.")
      | _ when List.length szs <> List.length $8 -> raise (Failure "Dimension not match.")
      | _ when mat_ty <> $6 -> raise (Failure "Matrix type not match.")
      | _ -> Matrix(mat_ty, get_list_of_len (List.length $8)))
      ), $2, Call("init_mat", IntLit(type_to_enum $6)::$8)) }
  | mat_typ ID ASSIGN expr {Init($1,$2,$4)}
  | func_typ ID ASSIGN expr {Init($1,$2,$4)}





rtyp:
    TVOID { Void }
  | mat_typ { $1 }
  | typ { $1 }
  | func_typ {$1}


/* function declaration (with formal parameters)*/
fdecl:
	rtyp FUNC ID LPAREN formal_opt RPAREN LBRACE stmt_list RBRACE
  { 
    {
      rtype = $1;
      fname = $3;
      formals = $5;
      body = $8
    } 
  }
  /* | FUNC ID LPAREN formal_opt RPAREN LBRACE func_stmt_list RBRACE
  { 
    {
      rtype = Pending ;
      fname = $2;
      formals = $4;
      body = $7
    } 
  } */

typid: 
    typ ID { ($1, $2) }
    /* TODO: CHAO */
  | mat_typ ID { ($1, $2) }
  | func_typ ID { ($1, $2) }


/* formal parameters in the function declaration */
formal_opt:
	 /* nothing */ { [] }
  | formal_list   { $1 }

formal_list:
	  typid  { [$1] }
  | typid COMMA formal_list { $1 :: $3 }

stmt_list: 
    /*nothing*/ { [] } 
  | stmt stmt_list { $1::$2 }

stmt:
    ext_expr SEMI                           { Expr($1) }
  | LBRACE stmt_list RBRACE                 { Block($2) }
  | IF LPAREN expr RPAREN LBRACE stmt_list RBRACE { If($3, Block($6), Block([])) }
  | IF LPAREN expr RPAREN LBRACE stmt_list RBRACE ELSE LBRACE stmt_list RBRACE 
  { If($3, Block($6), Block($10)) }
  | FOR LPAREN ext_expr SEMI ext_expr SEMI ext_expr RPAREN LBRACE stmt_list RBRACE  { For($3, $5, $7, Block($10)) }
/*  | FOR ID IN expr LBRACE stmt_list RBRACE  { For($2, $4, Block($6)) }*/
/*  | FOR ID IN expr COLON stmt               { For($2, $4, $6) }*/
  | WHILE LPAREN expr RPAREN LBRACE stmt_list RBRACE { While($3, Block($6)) }
  | BREAK SEMI                              { Break }
  | CONTINUE SEMI                           { Continue }
  | RETURN SEMI                             { Return(None) }
  | RETURN expr SEMI                        { Return(Some $2) }

ext_expr:
    lvdecl { $1 }
  | expr { $1 }
  | expr ASSIGN expr { Assign($1, $3) }

/*
 * Expressions that can evaluate a value as a result.
 * In other words, expressions that can be returned.
 */
expr:
  | expr OR     expr { Binop($1, Or,    $3) }
  | expr AND    expr { Binop($1, And,   $3) }
  | expr EQ     expr { Binop($1, Equal, $3) }
  | expr NEQ    expr { Binop($1, Neq,   $3) }
  | expr LT     expr { Binop($1, Less,  $3) }
  | expr LEQ    expr { Binop($1, Leq,   $3) }
  | expr GT     expr { Binop($1, Greater, $3) }
  | expr GEQ    expr { Binop($1, Geq,   $3) }
  | expr PLUS   expr { Binop($1, Add,   $3) }
  | expr MINUS  expr { Binop($1, Sub,   $3) }
  | expr TIMES  expr { Binop($1, Mult,  $3) }
  | expr DIVIDE expr { Binop($1, Div,   $3) }
  | expr MOD    expr { Binop($1, Mod,   $3) }
  | MINUS expr %prec NEG { Unop(Neg, $2) }
  | NOT expr         { Unop(Not, $2) }
  | ID LPAREN actual_opt RPAREN { Call($1, $3) }
  /* | TMAT LPAREN actual_opt RPAREN { Call($1, $3) } */


  | matrix_access { $1 }
/*
  | MID { MatInst($1) }
  | LBRACK actual_opt RBRACK { Arr($2) }
*/
  | LPAREN expr RPAREN { $2 }
  | ILIT { IntLit($1) }
  | BLIT { BoolLit($1) }
  | FLIT { FloatLit($1) }
  | SLIT { StrLit($1) }
  | ID { Var($1)}

matrix_access: 
   ID LBRACK expr RBRACK { GetIndex(Var($1), $3) }
  | ID LBRACK slice_expr RBRACK { GetSlice(Var($1), $3) }
  | matrix_access LBRACK expr RBRACK { GetIndex($1, $3) }
  | matrix_access LBRACK slice_expr RBRACK { GetSlice($1, $3) }

slice_expr:
   COLON { (None, None) }
  | expr COLON  { (Some $1, None) } 
  | COLON expr  { (None, Some $2) } 
  | expr COLON expr  { (Some $1, Some $3) } 

mat_def_expr:
    LBRACK actual_list RBRACK { Mat($2) }
  /* | LBRACK actual_opt RBRACK { Mat($2) } */
  | LBRACK mat_def_expr_list RBRACK { Mat($2) }

mat_def_expr_list:
    mat_def_expr { [$1] }
  | mat_def_expr COMMA mat_def_expr_list { $1::$3 }

/* return an "expr list" */
/* row_def_exprs:
    row_def_expr COMMA row_def_expr { [$1; $3] }
  | row_def_expr COMMA row_def_exprs { $1 :: $3 }

row_def_expr: actual_opt { Mat($1) } */

/* function calling (with actual parameters) */
/* also can be used in the matrix rows definition */
/* return an "expr list" */
actual_opt:
    /*nothing*/ { [] } 
  | actual_list  { $1 }

actual_list:
    expr { [$1] }
  | expr COMMA actual_list { $1 :: $3 }



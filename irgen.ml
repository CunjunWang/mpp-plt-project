module L = Llvm
module A = Ast
open Sast

module StringMap = Map.Make(String)

let debug_println (s: string) = 
	let debug = false in
	if debug then (print_string s)

type pos_to_jump = {
	(* for continue *)
	pos_begin_while: L.llbasicblock option;
	(* for break *)
	pos_end_while: L.llbasicblock option;
}

type symbol_table = {
	(* Variables bound in current block *)
	variables: L.llvalue StringMap.t ref;
	(* Enclosing scope *)
	parent: symbol_table option ref;
}

let rec print_scope (scope: symbol_table): unit =
	debug_println "================\n";
	let rec helper (scope: symbol_table): unit =  
		StringMap.iter (fun k v -> debug_println ("(" ^ k ^ "," ^ L.string_of_llvalue v ^ ")\n")) !(scope.variables);
		debug_println "--------------\n";
		match !(scope.parent) with
		| Some(parent_scope) -> helper parent_scope
		| _ -> ()
	in helper scope; debug_println "================\n"

(* find variable from the current scope upto the basic/root scope *)
let rec find_variable (name: string) (scope: symbol_table): L.llvalue =
	try
		(* Try to find the binding in the nearest block *)
		StringMap.find name !(scope.variables)
	with Not_found ->
		match !(scope.parent) with
		| Some(parent_scope) -> find_variable name parent_scope
		| _ -> raise (Failure ("Variable \"" ^ name ^ "\" not found"))

(* add variable (name, v) to the current scope *)
let add_variable (name: string) (v: L.llvalue) (scope: symbol_table): unit =
	scope.variables := StringMap.add name v !(scope.variables)
(*;
	debug_println ("\nafter add " ^ name ^ " :\n");
	print_scope scope
*)

let check_dup_variable (name: string) (scope: symbol_table): unit = 
	try
		(* Try to find the binding in the nearest block *)
		ignore(StringMap.find name !(scope.variables));
		raise (Failure ("the variable \"" ^ name ^ "\" has already been defined in the current scope"))
	with Not_found -> ()

(* link the child scope to the parent scope and return the child scope,
	which is the current scope *)
let link_scope (child_scope: symbol_table) (parent_scope: symbol_table): symbol_table =
	child_scope.parent := Some parent_scope;
	child_scope

(* create a new scope, typical used with link scope *)
let new_scope (): symbol_table =
	let s = { variables = ref StringMap.empty; parent = ref None } in s

(* move function main to the front, assume there is only one "main" *)
let main_to_front_list (li: sfunc_decl list): sfunc_decl list = 
	try
		(List.find (fun a -> a.sfname = "main") li)::(List.filter (fun a -> a.sfname <> "main") li)
	with Not_found -> li

let is_func (t: A.typ): bool =
	match t with
	| A.Fun(_) -> true
	| _ -> false

let is_matrix (t: A.typ): bool =
	match t with
	| A.Matrix(_) -> true
	| _ -> false

let is_int_matrix (t: A.typ): bool =
	match t with
	| A.Matrix(A.Int,_) -> true
	| _ -> false

let is_float_matrix (t: A.typ): bool =
	match t with
	| A.Matrix(A.Float,_) -> true
	| _ -> false

let is_bool_matrix (t: A.typ): bool =
	match t with
	| A.Matrix(A.Bool,_) -> true
	| _ -> false

(* translate : Sast.sprogram -> Llvm.module *)
(*(incs: string list) (g_vars: sexpr list) (funcs: sfunc_decl list)*)
let translate ((incs, g_vars, funcs): sprogram): L.llmodule =
	let context: L.llcontext = L.global_context () in

	(* Create the LLVM compilation module into which we will generate code *)
	let the_module: L.llmodule = L.create_module context "Mpp" in

	(* Declare some basic types *)
	let void_t: L.lltype = L.void_type context in
	let i32_t: L.lltype = L.i32_type context in
	let i8_t: L.lltype = L.i8_type context in
	let i1_t: L.lltype = L.i1_type context in
	let str_t: L.lltype = L.pointer_type i8_t in
	let f64_t: L.lltype = L.double_type context in
	let void_ptr_t: L.lltype = L.pointer_type i8_t in

	(* Declare struct Mat *)
	let struct_mat_t: L.lltype = L.named_struct_type context "Mat" in

	let _ = L.struct_set_body struct_mat_t 
		[| i32_t; i32_t; L.pointer_type i32_t; void_ptr_t; i32_t |] false in

	(* Return the LLVM type for a Mpp type *)
	let rec typ_to_ltype (t: A.typ): L.lltype =
		match t with
		| A.Void -> void_t
		| A.Int -> i32_t
		| A.Bool -> i1_t
		| A.Float -> f64_t
		| A.String -> str_t
		| A.Matrix(_) -> struct_mat_t
		| A.Fun(_, r_ty, formal_ty_li) ->
			let r_ty_ll: L.lltype = typ_to_ltype r_ty in
			let formal_ty_ll_li: L.lltype list = List.map (fun a -> typ_to_ltype a) formal_ty_li in
			L.pointer_type (L.function_type r_ty_ll (Array.of_list formal_ty_ll_li))
		| _ as ty -> raise (Failure ("cannot change this to lltype " ^ (A.string_of_typ ty)))
	(* Return the suitable LLVM type for a Mpp type *)
	and typ_to_suitable_ltype (t: A.typ): L.lltype = 
		match t with
		| A.Void -> void_t
		| A.Int -> i32_t
		| A.Bool -> i1_t
		| A.Float -> f64_t
		| A.String -> str_t
		| A.Matrix(_) -> L.pointer_type struct_mat_t
		| A.Fun(_, r_ty, formal_ty_li) ->
			let r_ty_ll: L.lltype = typ_to_ltype r_ty in
			let formal_ty_ll_li: L.lltype list = List.map (fun a -> typ_to_ltype a) formal_ty_li in
			L.pointer_type (L.function_type r_ty_ll (Array.of_list formal_ty_ll_li))
		| _ as ty -> raise (Failure ("cannot change this to lltype " ^ (A.string_of_typ ty)))
	in
(*
	let lltype_to_string (t: L.lltype): string = 
		match t with
		| void_t -> "void"
		| i32_t -> "int"
		| i1_t -> "bool"
		| f64_t -> "float"
		| struct_mat_t -> "mat"
	in
*)
	(* Delcare the built-in functions *)

	(* types of these functions *)
	let printf_func_t: L.lltype =
	L.var_arg_function_type i32_t [| L.pointer_type i8_t |] in
	let printf_func: L.llvalue =
	L.declare_function "printf" printf_func_t the_module in
	let print_matrix_func_t: L.lltype = L.function_type void_t
	[| L.pointer_type struct_mat_t |] in
	let print_matrix_func: L.llvalue =
	L.declare_function "print_matrix" print_matrix_func_t the_module in
	let assert_func_t: L.lltype = L.function_type (void_t) [| i1_t |] in
	let assert_func: L.llvalue = L.declare_function "assert" assert_func_t the_module in
	let assert_msg_func_t: L.lltype = L.function_type (void_t) [| L.pointer_type i8_t; i1_t |] in
	let assert_msg_func: L.llvalue = L.declare_function "assert_msg" assert_msg_func_t the_module in
	(* init a n-dim-all-zero-matrix with size d1*d2*...*dn *)
	(* void init_mat(struct Mat* m, int n, ...); *)
	let init_mat_func_t: L.lltype = L.var_arg_function_type void_t
	[| L.pointer_type struct_mat_t; i32_t |]
	(* init a n-dim matrix with array-list value initialization *)
	and init_mat_value_func_t: L.lltype = L.var_arg_function_type void_t
	[| L.pointer_type struct_mat_t; i32_t |]
	(* get the dimension matrix of a matrix *)
    (* struct Mat* get_dim(struct Mat* m); *)
	and get_dim_func_t: L.lltype = L.function_type void_t
	[| L.pointer_type struct_mat_t; L.pointer_type struct_mat_t |]
	(* get the element of index i *)
	and get_index_func_t: L.lltype = L.function_type void_ptr_t
	[| L.pointer_type struct_mat_t; L.pointer_type struct_mat_t; i32_t |]
	(* get the elements from index i to index j *)
	and get_slice_func_t: L.lltype = L.function_type void_t
	[| L.pointer_type struct_mat_t; L.pointer_type struct_mat_t; i32_t; i32_t |]
	(* matrix * matrix operation *)
	and matrix_operation_func_t: L.lltype = L.function_type (L.pointer_type struct_mat_t)
	[| L.pointer_type struct_mat_t; L.pointer_type struct_mat_t; i32_t|]
	(* matrix * int operation*)
	and matrix_int_operation_func_t: L.lltype = L.function_type (L.pointer_type struct_mat_t)
	[| L.pointer_type struct_mat_t; i32_t; i32_t|]
	and matrix_float_operation_func_t: L.lltype = L.function_type (L.pointer_type struct_mat_t)
	[| L.pointer_type struct_mat_t; f64_t; i32_t|]
	and matrix_bool_operation_func_t: L.lltype = L.function_type (L.pointer_type struct_mat_t)
	[| L.pointer_type struct_mat_t; i1_t; i32_t|]
	in
	let int_ptr_tstr: string = L.string_of_lltype (L.pointer_type i32_t) in
	let float_ptr_tstr: string = L.string_of_lltype (L.pointer_type f64_t) in
	let bool_ptr_tstr: string = L.string_of_lltype (L.pointer_type i1_t) in
	let mat_ptr_tstr: string = L.string_of_lltype (L.pointer_type struct_mat_t) in

	(* actual value of these functions *)

	let init_mat_func: L.llvalue = L.declare_function "init_mat" init_mat_func_t the_module
	and init_mat_value_func: L.llvalue = L.declare_function "init_mat_value" init_mat_value_func_t the_module
	and get_dim_func: L.llvalue = L.declare_function "get_dim" get_dim_func_t the_module
	and get_index_func: L.llvalue = L.declare_function "get_index" get_index_func_t the_module
	and get_slice_func: L.llvalue = L.declare_function "get_slice" get_slice_func_t the_module
	and matrix_operation_func: L.llvalue = L.declare_function "matrix_operation" matrix_operation_func_t the_module
	and matrix_int_operation_func: L.llvalue = L.declare_function "matrix_int_operation" matrix_int_operation_func_t the_module
	and matrix_float_operation_func: L.llvalue = L.declare_function "matrix_float_operation" matrix_float_operation_func_t the_module
	and matrix_bool_operation_func: L.llvalue = L.declare_function "matrix_bool_operation" matrix_bool_operation_func_t the_module
	in

	(* Define each function (arguments and return type)
		so we can call it even before we've created its body *)
	let fmap: (L.llvalue * sfunc_decl) StringMap.t =
		let function_decl (m: (L.llvalue * sfunc_decl) StringMap.t) (sfdecl: sfunc_decl) =
			let fname: string = sfdecl.sfname and formal_types: L.lltype array =
				Array.of_list (List.map (fun (t,_) -> typ_to_ltype t) sfdecl.sformals)
			in
			let ftype: L.lltype = L.function_type (typ_to_suitable_ltype sfdecl.srtype) formal_types in
			let fvalue: L.llvalue = L.define_function fname ftype the_module in
			StringMap.add fname (fvalue, sfdecl) m
		in
		List.fold_left function_decl StringMap.empty funcs
	in

	let is_pointer (pos_ptr: L.llvalue): bool =
		match pos_ptr with
		| _ when L.string_of_lltype (L.type_of pos_ptr) = mat_ptr_tstr -> debug_println "mat_S"; true
		| _ when L.string_of_lltype (L.type_of pos_ptr) = int_ptr_tstr -> debug_println "i32a"; true
		| _ when L.string_of_lltype (L.type_of pos_ptr) = float_ptr_tstr -> debug_println "f64"; true
		| _ when L.string_of_lltype (L.type_of pos_ptr) = bool_ptr_tstr -> debug_println "i1"; true
		| _ -> false
	in
	let is_matrix_pointer (pos_ptr: L.llvalue): bool =
		match pos_ptr with
		| _ when L.string_of_lltype (L.type_of pos_ptr) = mat_ptr_tstr -> debug_println "mat_S"; true
		| _ when L.string_of_lltype (L.type_of pos_ptr) = int_ptr_tstr -> debug_println "i32b"; false
		| _ when L.string_of_lltype (L.type_of pos_ptr) = float_ptr_tstr -> debug_println "f64b"; false
		| _ -> false
	in
	let is_int_pointer (pos_ptr: L.llvalue): bool =
		match pos_ptr with
		| _ when L.string_of_lltype (L.type_of pos_ptr) = mat_ptr_tstr -> debug_println "mat_S"; false
		| _ when L.string_of_lltype (L.type_of pos_ptr) = int_ptr_tstr -> debug_println "i32c"; true
		| _ -> false
	in
	let is_float_pointer (pos_ptr: L.llvalue): bool =
		match pos_ptr with
		| _ when L.string_of_lltype (L.type_of pos_ptr) = float_ptr_tstr -> debug_println "f64b"; true
		| _ when L.string_of_lltype (L.type_of pos_ptr) = mat_ptr_tstr -> debug_println "mat_S"; false
		| _ when L.string_of_lltype (L.type_of pos_ptr) = int_ptr_tstr -> debug_println "i32c"; false
		| _ -> false
	in
	let is_bool_pointer (pos_ptr: L.llvalue): bool =
		match pos_ptr with
		| _ when L.string_of_lltype (L.type_of pos_ptr) = bool_ptr_tstr -> debug_println "i1"; true
		| _ when L.string_of_lltype (L.type_of pos_ptr) = int_ptr_tstr -> debug_println "i32c"; false
		| _ -> false
	in
(* 	let is_int_or_float_pointer (pos_ptr: L.llvalue): bool =
		match pos_ptr with
		| _ when L.string_of_lltype (L.type_of pos_ptr) = mat_ptr_tstr -> debug_println "mat_S"; false
		| _ when L.string_of_lltype (L.type_of pos_ptr) = int_ptr_tstr -> debug_println "i32c"; true
		| _ when L.string_of_lltype (L.type_of pos_ptr) = float_ptr_tstr -> debug_println "f64b"; true
		| _ -> false
	in *)
	(* Create global scope *)
	let global_scope: symbol_table = new_scope () in

	let global_value_map: L.llvalue StringMap.t ref = ref StringMap.empty in

	let rec parse_global_expr ((ty, sx): sexpr): L.llvalue = 
		let err: string = ("Do not support " ^ (string_of_sexpr (ty, sx)) ^ " in global area.") in
		match sx with
		| SIntLit(i) -> L.const_int i32_t i
		| SBoolLit(b) -> L.const_int i1_t (if b then 1 else 0) 
		| SFloatLit(f) -> L.const_float f64_t f
		(*
		| SStrLit(s) -> L.const_string context s
		*)
		| SVar(name) -> 
			(try
				StringMap.find name !global_value_map
			with Not_found -> 
				raise (Failure ("No global variable has name: " ^ name)))
		| SUnop(op, se) -> 
			(match (ty, op) with
			| (A.Int, A.Neg) -> L.const_neg
			| (A.Float, A.Neg) -> L.const_fneg
			| (A.Bool, A.Not) -> L.const_not
			| _ -> raise (Failure err)) (parse_global_expr se)
		| SBinop(se1, op, se2) ->
			(match (ty, op) with
			| (A.Int, A.Add) -> L.const_add
			| (A.Int, A.Sub) -> L.const_sub
			| (A.Int, A.Mult) -> L.const_mul
			| (A.Int, A.Div) -> L.const_sdiv
			| (A.Int, A.Equal) -> L.const_icmp L.Icmp.Eq
			| (A.Int, A.Neq) -> L.const_icmp L.Icmp.Ne
			| (A.Int, A.Less) -> L.const_icmp L.Icmp.Slt
			| (A.Int, A.Greater) -> L.const_icmp L.Icmp.Sgt
			| (A.Int, A.Leq) -> L.const_icmp L.Icmp.Sle
			| (A.Int, A.Geq) -> L.const_icmp L.Icmp.Sge
			| (A.Int, A.Mod) -> L.const_srem 

			| (A.Float, A.Add) -> L.const_fadd
			| (A.Float, A.Sub) -> L.const_fsub
			| (A.Float, A.Mult) -> L.const_fmul
			| (A.Float, A.Div) -> L.const_fdiv
			| (A.Float, A.Equal) -> L.const_fcmp L.Fcmp.Oeq
			| (A.Float, A.Neq) -> L.const_fcmp L.Fcmp.One
			| (A.Float, A.Less) -> L.const_fcmp L.Fcmp.Olt
			| (A.Float, A.Greater) -> L.const_fcmp L.Fcmp.Ogt
			| (A.Float, A.Leq) -> L.const_fcmp L.Fcmp.Ole
			| (A.Float, A.Geq) -> L.const_fcmp L.Fcmp.Oge
			| (A.Float, A.Mod) -> L.const_frem 

			| (A.Bool, A.And) -> L.const_and
			| (A.Bool, A.Or) -> L.const_or

			| _ -> raise (Failure err)) (parse_global_expr se1) (parse_global_expr se2)
		| _ -> raise (Failure err)
	in

	(* Add global variables to the global scope *)
	let add_global_var ((ty, var_sx): sexpr): unit =
		match var_sx with
		| SInit(var_name, se) -> 
			let init: L.llvalue = parse_global_expr se in
			global_value_map := StringMap.add var_name init !global_value_map;
			add_variable var_name (L.define_global var_name init the_module) global_scope
		| _ -> 
			raise (Failure ("Do not support " ^ (string_of_sexpr (ty, var_sx)) ^ " in global area."))
	in

	let _ = List.iter add_global_var g_vars in	

	(* Fill in the body of the given function *)
	let build_function_body (gl_scope: symbol_table) (fmap: (L.llvalue * sfunc_decl) StringMap.t)
	(sfdecl: sfunc_decl): unit =
		let (the_function, _): L.llvalue * sfunc_decl = StringMap.find sfdecl.sfname fmap in
		let builder: L.llbuilder = L.builder_at_end context (L.entry_block the_function) in

		let zero: L.llvalue = (L.const_int i32_t 0) in
		let mone: L.llvalue = (L.const_int i32_t (-1)) in

		let get_proper_type_to_pass (builder: L.llbuilder) (a: L.llvalue): L.llvalue = 
			if is_pointer a then L.build_load a "deref_farg" builder else a in

		(* Construct code for an expression; return its value *)
		let rec build_expr (scope: symbol_table) (fmap: (L.llvalue * sfunc_decl) StringMap.t)
		(builder: L.llbuilder) ((ty, e): sexpr): L.llvalue =
			match e with
			| SIntLit(i) -> L.const_int i32_t i
			| SFloatLit(f) -> debug_println (string_of_float f); L.const_float f64_t f
			| SBoolLit(b) -> L.const_int i1_t (if b then 1 else 0)
			| SStrLit(s) -> debug_println s; L.build_global_stringptr s "STR" builder 
			| SVar(s) -> 
				if is_func ty then 
					try
						let _ = debug_println ("\n\n\nsfdfadsfa:" ^ s) in
						let (fdef, fdecl): L.llvalue * sfunc_decl = StringMap.find s fmap in fdef					
					with Not_found -> 
						(* this function is not a global defined function *)
						let func_ptr: L.llvalue = find_variable s scope in func_ptr
				else
					let pos_ptr: L.llvalue = find_variable s scope in
					if is_matrix_pointer pos_ptr then 
						pos_ptr
					else
						L.build_load pos_ptr s builder
			| SAssign(se1, se2) -> 
				(* TODO: lhs of assign is not string, need to wait for the matrix expression *)
				let tmp_rhs: L.llvalue = build_expr scope fmap builder se2 in
				let lhs: L.llvalue = build_expr scope fmap builder se1 in
				let rhs: L.llvalue = (match tmp_rhs with
				| _ when (is_pointer tmp_rhs) -> L.build_load tmp_rhs "tmp_rhs" builder
				| _ (*when not (is_pointer tmp_rhs)*) -> tmp_rhs) in
				(match se1 with
				| (ty, SVar(vname)) -> 
					ignore(L.build_store rhs (find_variable vname scope) builder); rhs
				| _ -> ignore(L.build_store rhs lhs builder); rhs)
			| SBinop (se1, op, se2) ->
				let e1': L.llvalue = build_expr scope fmap builder se1
				and e2': L.llvalue = build_expr scope fmap builder se2 in
				let actual_e1: L.llvalue = 
					(match e1' with
					| _ when (is_int_pointer e1') -> L.build_load e1' "e1'" builder
					| _ when (is_float_pointer e1') -> L.build_load e1' "e1'" builder
					| _ when (is_bool_pointer e1') -> L.build_load e1' "e1'" builder
					| _ -> e1') in
				let actual_e2: L.llvalue = 
					(match e2' with
					| _ when (is_int_pointer e2') -> L.build_load e2' "e2'" builder
					| _ when (is_float_pointer e2') -> L.build_load e2' "e2'" builder
					| _ when (is_bool_pointer e2') -> L.build_load e2' "e2'" builder
					| _ -> e2') in
				let t1: A.typ = fst se1 and t2: A.typ = fst se2 in
				if (t1 = t2) then 
					(match e1' with
					(* when lhs and rhs are both not pointer,
						which means they are both not matrix(basic type), add them directly *)
					| _ when (is_int_pointer e1' || t2 = A.Int)  -> 
						((match op with
						 | A.Add     -> L.build_add
						 | A.Sub     -> L.build_sub
						 | A.Mult    -> L.build_mul
						 | A.Div     -> L.build_sdiv
						 | A.Equal   -> L.build_icmp L.Icmp.Eq
						 | A.Neq     -> L.build_icmp L.Icmp.Ne
						 | A.Less    -> L.build_icmp L.Icmp.Slt
						 | A.Greater -> L.build_icmp L.Icmp.Sgt
						 | A.Leq     -> L.build_icmp L.Icmp.Sle
						 | A.Geq     -> L.build_icmp L.Icmp.Sge
						 | A.Mod     -> L.build_srem
						 | _         -> raise (Failure "operation not support")
						) actual_e1 actual_e2 "tmp" builder)
					| _ when (is_float_pointer e1' || t2 = A.Float)  -> 
						((match op with
						 | A.Add     -> L.build_fadd
						 | A.Sub     -> L.build_fsub
						 | A.Mult    -> L.build_fmul
						 | A.Div     -> L.build_fdiv
						 | A.Equal   -> L.build_fcmp L.Fcmp.Oeq
						 | A.Neq     -> L.build_fcmp L.Fcmp.One
						 | A.Less    -> L.build_fcmp L.Fcmp.Olt
						 | A.Greater -> L.build_fcmp L.Fcmp.Ogt
						 | A.Leq     -> L.build_fcmp L.Fcmp.Ole
						 | A.Geq     -> L.build_fcmp L.Fcmp.Oge
						 | A.Mod     -> L.build_frem
						 | _         -> raise (Failure "operation not support")
						) actual_e1 actual_e2 "tmp" builder)
					| _ when (is_bool_pointer e1' || t1 = A.Bool)  -> 
						((match op with
						 | A.And     -> L.build_and
						 | A.Or      -> L.build_or
						 | _         -> raise (Failure "operation not support")
						) actual_e1 actual_e2 "tmp" builder)
					(* when lhs and rhs are both matrix(matrix pointer), add them directly *)
					| _ when (is_matrix_pointer e1') -> 
						(match op with
						 | A.Add -> let mat_val_ptr: L.llvalue =  L.build_call matrix_operation_func 
							[| e1'; e2'; L.const_int i32_t 1 |] "mat_operation_result" builder in mat_val_ptr
						 | A.Sub -> let mat_val_ptr: L.llvalue =  L.build_call matrix_operation_func 
							[| e1'; e2'; L.const_int i32_t 2 |] "mat_operation_result" builder in mat_val_ptr
						 | A.Mult -> let mat_val_ptr: L.llvalue =  L.build_call matrix_operation_func 
							[| e1'; e2'; L.const_int i32_t 3 |] "mat_operation_result" builder in mat_val_ptr
						 | A.Div -> let mat_val_ptr: L.llvalue =  L.build_call matrix_operation_func 
							[| e1'; e2'; L.const_int i32_t 4 |] "mat_operation_result" builder in mat_val_ptr
						 | A.Mod -> let mat_val_ptr: L.llvalue =  L.build_call matrix_operation_func 
							[| e1'; e2'; L.const_int i32_t 5 |] "mat_operation_result" builder in mat_val_ptr
						 | A.And -> let mat_val_ptr: L.llvalue =  L.build_call matrix_operation_func 
							[| e1'; e2'; L.const_int i32_t 6 |] "mat_operation_result" builder in mat_val_ptr
						 | A.Or  -> let mat_val_ptr: L.llvalue =  L.build_call matrix_operation_func 
							[| e1'; e2'; L.const_int i32_t 7 |] "mat_operation_result" builder in mat_val_ptr
						 | _     -> raise (Failure "operation not support"))
					| _ -> raise (Failure "operation not support for the given type"))
			    else (
			    	match e1' with
			    	(* when lhs is basic type *)
					| _ when not (is_pointer e1') -> 
						(match e2' with
						| _ when (is_int_pointer e2') -> 
							((match op with
							 | A.Add     -> L.build_add
							 | A.Sub     -> L.build_sub
							 | A.Mult    -> L.build_mul
							 | A.Div     -> L.build_sdiv
							 | A.Equal   -> L.build_icmp L.Icmp.Eq
							 | A.Neq     -> L.build_icmp L.Icmp.Ne
							 | A.Less    -> L.build_icmp L.Icmp.Slt
							 | A.Greater -> L.build_icmp L.Icmp.Sgt
							 | A.Leq     -> L.build_icmp L.Icmp.Sle
							 | A.Geq     -> L.build_icmp L.Icmp.Sge
							 | A.Mod     -> L.build_srem
							 | _         -> raise (Failure "operation not support")
							) e1' actual_e2 "tmp" builder)
						| _ when (is_float_pointer e2') -> 
							((match op with
							 | A.Add     -> L.build_fadd
							 | A.Sub     -> L.build_fsub
							 | A.Mult    -> L.build_fmul
							 | A.Div     -> L.build_fdiv
							 | A.Equal   -> L.build_fcmp L.Fcmp.Oeq
							 | A.Neq     -> L.build_fcmp L.Fcmp.One
							 | A.Less    -> L.build_fcmp L.Fcmp.Olt
							 | A.Greater -> L.build_fcmp L.Fcmp.Ogt
							 | A.Leq     -> L.build_fcmp L.Fcmp.Ole
							 | A.Geq     -> L.build_fcmp L.Fcmp.Oge
							 | A.Mod     -> L.build_frem
							 | _         -> raise (Failure "operation not support")
							) e1' actual_e2 "tmp" builder)
						| _ when (is_bool_pointer e2') -> 
							((match op with
							 | A.And     -> L.build_and
							 | A.Or      -> L.build_or
							 | _         -> raise (Failure "operation not support")
							) e1' actual_e2 "tmp" builder)
						| _ when (is_matrix_pointer e2' && is_int_matrix(t2)) -> 
							(match op with
							| A.Add  -> let mat_val_ptr: L.llvalue =  L.build_call matrix_int_operation_func 
								[| actual_e2; e1'; L.const_int i32_t 1 |] "mat_int_operation_result" builder in mat_val_ptr
							| A.Mult -> let mat_val_ptr: L.llvalue =  L.build_call matrix_int_operation_func 
								[| actual_e2; e1'; L.const_int i32_t 3 |] "mat_int_operation_result" builder in mat_val_ptr
							| _      -> raise (Failure "operation not support"))
						| _ when (is_matrix_pointer e2' && is_float_matrix(t2)) -> 
							(match op with
							| A.Add  -> let mat_val_ptr: L.llvalue =  L.build_call matrix_float_operation_func 
								[| actual_e2; e1'; L.const_int i32_t 1 |] "mat_float_operation_result" builder in mat_val_ptr
							| A.Mult -> let mat_val_ptr: L.llvalue =  L.build_call matrix_float_operation_func 
								[| actual_e2; e1'; L.const_int i32_t 3 |] "mat_float_operation_result" builder in mat_val_ptr
							| _      -> raise (Failure "operation not support"))
						| _ when (is_matrix_pointer e2' && is_bool_matrix(t2)) -> 
							(match op with
							| A.And -> let mat_val_ptr: L.llvalue =  L.build_call matrix_bool_operation_func 
								[| actual_e2; e1'; L.const_int i32_t 1 |] "mat_bool_operation_result" builder in mat_val_ptr
							| A.Or  -> let mat_val_ptr: L.llvalue =  L.build_call matrix_bool_operation_func 
								[| actual_e2; e1'; L.const_int i32_t 2 |] "mat_bool_operation_result" builder in mat_val_ptr
							| _         -> raise (Failure "operation not support"))
						| _ -> raise (Failure "operation not support for the given type"))
					| _ when (is_int_pointer e1' || is_float_pointer e1' || is_bool_pointer e1') -> 
						(match e2' with
						| _ when (is_int_pointer e1') -> 
							((match op with
							 | A.Add     -> L.build_add
							 | A.Sub     -> L.build_sub
							 | A.Mult    -> L.build_mul
							 | A.Div     -> L.build_sdiv
							 | A.Equal   -> L.build_icmp L.Icmp.Eq
							 | A.Neq     -> L.build_icmp L.Icmp.Ne
							 | A.Less    -> L.build_icmp L.Icmp.Slt
							 | A.Greater -> L.build_icmp L.Icmp.Sgt
							 | A.Leq     -> L.build_icmp L.Icmp.Sle
							 | A.Geq     -> L.build_icmp L.Icmp.Sge
							 | A.Mod     -> L.build_srem
							 | _         -> raise (Failure "operation not support")
							) actual_e1 actual_e2 "tmp" builder)
						| _ when (is_float_pointer e1') -> 
							((match op with
							 | A.Add     -> L.build_fadd
							 | A.Sub     -> L.build_fsub
							 | A.Mult    -> L.build_fmul
							 | A.Div     -> L.build_fdiv
							 | A.Equal   -> L.build_fcmp L.Fcmp.Oeq
							 | A.Neq     -> L.build_fcmp L.Fcmp.One
							 | A.Less    -> L.build_fcmp L.Fcmp.Olt
							 | A.Greater -> L.build_fcmp L.Fcmp.Ogt
							 | A.Leq     -> L.build_fcmp L.Fcmp.Ole
							 | A.Geq     -> L.build_fcmp L.Fcmp.Oge
							 | A.Mod     -> L.build_frem
							 | _         -> raise (Failure "operation not support")
							) actual_e1 actual_e2 "tmp" builder)
						| _ when (is_bool_pointer e1') -> 
							((match op with
							 | A.And     -> L.build_and
							 | A.Or      -> L.build_or
							 | _         -> raise (Failure "operation not support")
							) actual_e1 actual_e2 "tmp" builder)
						| _ when (is_matrix_pointer e2' && is_float_matrix(t2)) -> 
							(match op with
							 | A.Add  -> let mat_val_ptr: L.llvalue =  L.build_call matrix_float_operation_func 
								[| actual_e2; actual_e1; L.const_int i32_t 1 |] "mat_float_operation_result" builder in mat_val_ptr
							 | A.Mult -> let mat_val_ptr: L.llvalue =  L.build_call matrix_float_operation_func 
								[| actual_e2; actual_e1; L.const_int i32_t 3 |] "mat_float_operation_result" builder in mat_val_ptr
							 | _      -> raise (Failure "operation not support"))
						| _ when (is_matrix_pointer e2' && is_int_matrix(t2)) -> 
							(match op with
							 | A.Add  -> let mat_val_ptr: L.llvalue =  L.build_call matrix_int_operation_func 
								[| actual_e2; actual_e1; L.const_int i32_t 1 |] "mat_int_operation_result" builder in mat_val_ptr
							 | A.Mult -> let mat_val_ptr: L.llvalue =  L.build_call matrix_int_operation_func 
								[| actual_e2; actual_e1; L.const_int i32_t 3 |] "mat_int_operation_result" builder in mat_val_ptr
							 | _      -> raise (Failure "operation not support"))
						| _ when (is_matrix_pointer e2' && is_bool_matrix(t2)) -> 
							(match op with
							 | A.And -> let mat_val_ptr: L.llvalue =  L.build_call matrix_bool_operation_func 
								[| actual_e2; actual_e1; L.const_int i32_t 1 |] "mat_bool_operation_result" builder in mat_val_ptr
							 | A.Or  -> let mat_val_ptr: L.llvalue =  L.build_call matrix_bool_operation_func 
								[| actual_e2; actual_e1; L.const_int i32_t 2 |] "mat_bool_operation_result" builder in mat_val_ptr
							 | _     -> raise (Failure "operation not support"))
						| _  -> raise (Failure "operation not support for the given type"))

					| _ when (is_matrix_pointer e1' && is_int_matrix(t1)) -> 
						(match op with
						 | A.Add -> let mat_val_ptr: L.llvalue =  L.build_call matrix_int_operation_func 
							[| e1'; actual_e2; L.const_int i32_t 1 |] "mat_int_operation_result" builder in mat_val_ptr
						 | A.Sub -> let mat_val_ptr: L.llvalue =  L.build_call matrix_int_operation_func 
							[| e1'; actual_e2; L.const_int i32_t 2 |] "mat_int_operation_result" builder in mat_val_ptr
						 | A.Mult -> let mat_val_ptr: L.llvalue =  L.build_call matrix_int_operation_func 
							[| e1'; actual_e2; L.const_int i32_t 3 |] "mat_int_operation_result" builder in mat_val_ptr
						 | A.Div -> let mat_val_ptr: L.llvalue =  L.build_call matrix_int_operation_func 
							[| e1'; actual_e2; L.const_int i32_t 4 |] "mat_int_operation_result" builder in mat_val_ptr
						 | A.Mod -> let mat_val_ptr: L.llvalue =  L.build_call matrix_int_operation_func 
							[| e1'; actual_e2; L.const_int i32_t 5 |] "mat_int_operation_result" builder in mat_val_ptr
						 | _     -> raise (Failure "operation not support"))
					| _ when (is_matrix_pointer e1' && is_float_matrix(t1)) -> 
						(match op with
						 | A.Add -> let mat_val_ptr: L.llvalue =  L.build_call matrix_float_operation_func 
							[| e1'; actual_e2; L.const_int i32_t 1 |] "mat_float_operation_result" builder in mat_val_ptr
						 | A.Sub -> let mat_val_ptr: L.llvalue =  L.build_call matrix_float_operation_func 
							[| e1'; actual_e2; L.const_int i32_t 2 |] "mat_float_operation_result" builder in mat_val_ptr
						 | A.Mult -> let mat_val_ptr: L.llvalue =  L.build_call matrix_float_operation_func 
							[| e1'; actual_e2; L.const_int i32_t 3 |] "mat_float_operation_result" builder in mat_val_ptr
						 | A.Div -> let mat_val_ptr: L.llvalue =  L.build_call matrix_float_operation_func 
							[| e1'; actual_e2; L.const_int i32_t 4 |] "mat_float_operation_result" builder in mat_val_ptr
						 | _     -> raise (Failure "operation not support"))
					| _ when (is_matrix_pointer e1' && is_bool_matrix(t1)) -> 
						(match op with
						 | A.And -> let mat_val_ptr: L.llvalue =  L.build_call matrix_bool_operation_func 
							[| e1'; actual_e2; L.const_int i32_t 1 |] "mat_bool_operation_result" builder in mat_val_ptr
						 | A.Or  -> let mat_val_ptr: L.llvalue =  L.build_call matrix_bool_operation_func 
							[| e1'; actual_e2; L.const_int i32_t 2 |] "mat_bool_operation_result" builder in mat_val_ptr
						 | _         -> raise (Failure "operation not support"))
					| _ -> raise (Failure "operation not support for the given type"))
			| SUnop (op, se) ->
				let e': L.llvalue = build_expr scope fmap builder se in
				let actual_e': L.llvalue = 
					(match e' with
					| _ when (is_int_pointer e') -> L.build_load e' "e'" builder
					| _ when (is_float_pointer e') -> L.build_load e' "e'" builder
					| _ when (is_bool_pointer e') -> L.build_load e' "e'" builder
					| _ -> e') in
				(match op with
				 | A.Neg -> (match ty with 
				   			| A.Int   -> L.build_neg
				   			| A.Float -> L.build_fneg
				   			| _       -> raise (Failure "operation not support"))
				 | A.Not -> L.build_not) actual_e' "tmp" builder
			| SCall ("printf", args_li) ->
				let args_li': L.llvalue list = List.map (fun e -> build_expr scope fmap builder e) args_li in
				let deref (pos_ptr: L.llvalue): L.llvalue = 
					if is_pointer pos_ptr then
						L.build_load pos_ptr "deref_pos_ptr" builder
					else pos_ptr
				in
				let de_args: L.llvalue list = (List.hd args_li')::(List.map deref (List.tl args_li')) in
				L.build_call printf_func (Array.of_list de_args) "printf" builder
			| SCall("init_mat", args_li) -> 
				let raw_args_li: L.llvalue list = List.map (fun e -> build_expr scope fmap builder e) args_li in
				let args_li': L.llvalue list = List.map (get_proper_type_to_pass builder) raw_args_li in
				let len: int = List.length args_li' in
				let new_mat_ptr: L.llvalue = L.build_alloca struct_mat_t "mat" builder in
				ignore (L.build_call init_mat_func 
					(Array.of_list (new_mat_ptr::(L.const_int i32_t len)::args_li')) 
					"" builder);
				L.build_load new_mat_ptr "new_mat" builder
			| SCall("get_dim", args_li) ->
				let args_li': L.llvalue list = List.map (fun e -> build_expr scope fmap builder e) args_li in
				let new_mat_ptr: L.llvalue = L.build_alloca struct_mat_t "mat" builder in
				ignore (L.build_call get_dim_func 
					(Array.of_list ((List.hd args_li')::new_mat_ptr::[])) "" builder);
				L.build_load new_mat_ptr "new_mat" builder
			| SCall("print_matrix", args_li) ->
				let args_li': L.llvalue list = List.map (fun e -> build_expr scope fmap builder e) args_li in
				L.build_call print_matrix_func (Array.of_list args_li') "" builder
			| SCall("assert", args_li) -> 
				let raw_args_li: L.llvalue list = List.map (fun e -> build_expr scope fmap builder e) args_li in
				let args_li': L.llvalue list = List.map (get_proper_type_to_pass builder) raw_args_li in
				L.build_call assert_func (Array.of_list args_li') "" builder
			| SCall("assert_msg", args_li) -> 
				let raw_args_li: L.llvalue list = List.map (fun e -> build_expr scope fmap builder e) args_li in
				let args_li': L.llvalue list = List.map (get_proper_type_to_pass builder) raw_args_li in
				L.build_call assert_msg_func (Array.of_list args_li') "" builder
			| SCall(fname, args) ->
				let fdef: L.llvalue = 
				(
				try
					let (fdef', fdecl): L.llvalue * sfunc_decl = StringMap.find fname fmap in
					fdef'
				with Not_found ->
					(* maybe fname is not a function name, but a variable name *)
					(
						try 
							let tmp: L.llvalue = find_variable fname scope in
							L.build_load tmp "deref_func" builder
						with Failure(f) -> 
							raise (Failure ("unrecognized function " ^ fname))
					)
				) in
				let raw_llargs: L.llvalue list = List.rev (List.map (build_expr scope fmap builder) (List.rev args)) in
				let llargs: L.llvalue list = List.map (get_proper_type_to_pass builder) raw_llargs in
				if ty = A.Void then
					L.build_call fdef (Array.of_list llargs) "" builder
				else
					L.build_call fdef (Array.of_list llargs) (fname ^ "_result") builder
			| SInit (vname, e) ->
				(* the return type of this match does not matter *)
				let _ = check_dup_variable vname scope in 
				let e': L.llvalue = build_expr scope fmap builder e in
				if is_func ty then 
				begin
					add_variable vname e' scope;
					e'					
				end
				else
				begin
					let pos_ptr: L.llvalue =  L.build_alloca (typ_to_ltype ty) vname builder in
					if is_pointer e' then 
					begin
						let drefe: L.llvalue = L.build_load e' "xxderef" builder in
						ignore(L.build_store drefe pos_ptr builder); add_variable vname pos_ptr scope; drefe					
					end
					else
					begin
						ignore(L.build_store e' pos_ptr builder); add_variable vname pos_ptr scope; e'					
					end				 	
				end 
			| SGetIndex (se1, se2) ->
				let mat_struct_p: L.llvalue = build_expr scope fmap builder se1 in
				let index: L.llvalue = build_expr scope fmap builder se2 in
				let g_i_tmp_mat_ptr: L.llvalue = L.build_alloca struct_mat_t "mat" builder in
				let fst_ele_val_ptr: L.llvalue = L.build_call get_index_func 
					[| mat_struct_p; g_i_tmp_mat_ptr; index |] "get_index_result" builder in
				if is_matrix ty then (* when expect a matrix, return a matrix *)
					g_i_tmp_mat_ptr
				else (* when get index on 1-d matrix, return first element ptr *)
					(match ty with
					| A.Int -> L.build_bitcast fst_ele_val_ptr (L.pointer_type i32_t) "casted_int" builder
					| A.Float -> L.build_bitcast fst_ele_val_ptr (L.pointer_type f64_t) "casted_float" builder
					| A.Bool -> L.build_bitcast fst_ele_val_ptr (L.pointer_type i1_t) "casted_bool" builder
					| _ -> raise (Failure "not support"))
			| SGetSlice (se, (None, None)) ->
				let mat_struct_p: L.llvalue = build_expr scope fmap builder se in
				let g_s_tmp_mat_ptr: L.llvalue = L.build_alloca struct_mat_t "mat" builder in
				ignore (L.build_call get_slice_func 
					[| mat_struct_p; g_s_tmp_mat_ptr; zero; mone |] "" builder);
				g_s_tmp_mat_ptr
			| SGetSlice (se, (Some sea, None)) ->
				let mat_struct_p: L.llvalue = build_expr scope fmap builder se in
				let g_s_tmp_mat_ptr: L.llvalue = L.build_alloca struct_mat_t "mat" builder in
				let st: L.llvalue = build_expr scope fmap builder sea in
				ignore (L.build_call get_slice_func 
					[| mat_struct_p; g_s_tmp_mat_ptr; st; mone |] "" builder);
				g_s_tmp_mat_ptr
			| SGetSlice (se, (None, Some seb)) ->
				let mat_struct_p: L.llvalue = build_expr scope fmap builder se in
				let g_s_tmp_mat_ptr: L.llvalue = L.build_alloca struct_mat_t "mat" builder in
				let en: L.llvalue = build_expr scope fmap builder seb in
				ignore (L.build_call get_slice_func 
					[| mat_struct_p; g_s_tmp_mat_ptr; zero; en |] "" builder);
				g_s_tmp_mat_ptr
			| SGetSlice (se, (Some sea, Some seb)) ->
				let mat_struct_p: L.llvalue = build_expr scope fmap builder se in
				let g_s_tmp_mat_ptr: L.llvalue = L.build_alloca struct_mat_t "mat" builder in
				let st: L.llvalue = build_expr scope fmap builder sea in
				let en: L.llvalue = build_expr scope fmap builder seb in
				ignore (L.build_call get_slice_func 
					[| mat_struct_p; g_s_tmp_mat_ptr; st; en |] "" builder);
				g_s_tmp_mat_ptr
			| SMat (l) -> 
				let rec find_dim l = (match l with
									| a :: b -> (match a with
												| (a1, a2) -> (match a2 with 
															 | SMat(ma) -> 1 + find_dim ma
															 | _ -> 1))
									| _ -> 0) in
				let num_dim: int = find_dim l + 1 in
				let rec get_dims l = (match l with
									| a :: b -> (match a with
												| (a1, a2) -> (match a2 with 
															 | SMat(ma) -> List.cons (List.length l) (get_dims ma)
															 | _ -> List.cons (List.length l) []))
									| _ -> []) in
				let dim_sizs: int list = get_dims l in
				let rec get_elem l = (match l with
									| a :: b -> (match a with
												| (a1, a2) -> (match a2 with
															| SMat(ma) -> List.append (get_elem ma) (get_elem b)
															| _ -> l))
									| [] -> []) in
				let elements: sexpr list = get_elem l in
				let elem_typ: L.llvalue =
					(match ty with
					| A.Matrix(A.Int,_) -> L.const_int i32_t 4
					| A.Matrix(A.Float,_) -> L.const_int i32_t 8
					| A.Matrix(A.Bool,_) -> L.const_int i32_t 1
					| _ -> raise (Failure("should be a matrix type"))) in
				(* let elem_typ: L.llvalue = (match elements with
										| a :: _ -> (match a with
													| (a1, a2) -> (match a2 with
																| SIntLit(_) -> L.const_int i32_t 4
																| SFloatLit(_) -> L.const_int i32_t 8
																| SBoolLit(_) -> L.const_int i32_t 1))) in *)
				let actual_sizs: L.llvalue list = List.map (fun e -> L.const_int i32_t e) dim_sizs in
				let args_li': L.llvalue list = List.map (fun e -> build_expr scope fmap builder e) elements in
				let new_mat_ptr: L.llvalue = L.build_alloca struct_mat_t "mat" builder in
				ignore (L.build_call init_mat_value_func 
					(Array.of_list (new_mat_ptr::(L.const_int i32_t num_dim)::elem_typ::(List.append actual_sizs args_li'))) 
					"" builder);
				L.build_load new_mat_ptr "new_mat" builder
			(*raise (Failure "not finish")*) (* TODO: init list and placeholder *)
		in

		(* Deprecated *)
		(* Build global variables 
		let _ = (if sfdecl.sfname = "main" then
			List.iter (fun se -> ignore (build_expr gl_scope fmap builder se)) g_vars
			else ())
		in
		*)

		(* basic/root scope of current function *)
		let fun_scope: symbol_table = link_scope (new_scope ()) gl_scope in
		let add_formal ((ty, pname): A.typ * string) (pa: L.llvalue): unit =
			L.set_value_name pname pa;
			let pos = L.build_alloca (typ_to_ltype ty) pname builder in
			ignore (L.build_store pa pos builder);
			ignore (add_variable pname pos fun_scope) in
		let _ = List.iter2 add_formal sfdecl.sformals (Array.to_list (L.params the_function)) in

		(* LLVM insists each basic block end with exactly one "terminator"
		   instruction that transfers control.  This function runs "instr builder"
		   if the current block does not already have a terminator.  Used,
		   e.g., to handle the "fall off the end of the function" case. *)
		let add_terminal (builder: L.llbuilder) (instr: L.llbuilder->L.llvalue): unit=
			match L.block_terminator (L.insertion_block builder) with
			| Some _ -> ()
			| None -> ignore (instr builder) in

		(* Build the code for the given statement; return the builder for
		the statement's successor (i.e., the next instruction will be built
		after the one generated by this call) *)

		let rec build_stmt (scope: symbol_table) (fmap: (L.llvalue * sfunc_decl) StringMap.t)
		(ptj: pos_to_jump) (builder: L.llbuilder) (st: sstmt): L.llbuilder =
			match st with
			  (* build all stmts in the list sl, return a builder *)
			| SBlock(sl) -> 
				List.fold_left (build_stmt (link_scope (new_scope ()) scope) fmap ptj) builder sl
			| SExpr(se) -> 
				ignore(build_expr scope fmap builder se); builder
			| SReturn(None) -> 
				ignore(L.build_ret_void builder); builder
			| SReturn(Some se) -> 
				let raw_rtn_val: L.llvalue = (build_expr scope fmap builder se) in
				let rtn_val: L.llvalue = 
				if is_pointer raw_rtn_val && not (is_matrix_pointer raw_rtn_val) then 
					L.build_load raw_rtn_val "deref_rtn" builder 
				else 
					raw_rtn_val in
				ignore(L.build_ret rtn_val builder); builder
			| SIf(predicate, then_stmt, else_stmt) ->
				let bool_val: L.llvalue = build_expr scope fmap builder predicate in

				let then_bb: L.llbasicblock = L.append_block context "then" the_function in
				let then_bb_b = (build_stmt scope fmap ptj (L.builder_at_end context then_bb) then_stmt) in
				let else_bb: L.llbasicblock = L.append_block context "else" the_function in
				let else_bb_b = (build_stmt scope fmap ptj (L.builder_at_end context else_bb) else_stmt) in

				let end_bb: L.llbasicblock = L.append_block context "if_end" the_function in
				let build_br_end = L.build_br end_bb in (* partial function *)
				add_terminal then_bb_b build_br_end;
				add_terminal else_bb_b build_br_end;
				(*
				add_terminal (L.builder_at_end context then_bb) build_br_end;
				add_terminal (L.builder_at_end context else_bb) build_br_end;
				*)

				ignore(L.build_cond_br bool_val then_bb else_bb builder);
				L.builder_at_end context end_bb

			| SWhile (predicate, body) ->
				let while_bb: L.llbasicblock = L.append_block context "while" the_function in
				let build_br_while: L.llbuilder->L.llvalue = L.build_br while_bb in (* partial function *)
				ignore (build_br_while builder);
				let while_builder = L.builder_at_end context while_bb in
				let bool_val = build_expr scope fmap while_builder predicate in

				let end_bb = L.append_block context "while_end" the_function in

				let body_bb = L.append_block context "while_body" the_function in
				let new_ptj: pos_to_jump = {pos_begin_while = Some while_bb; pos_end_while = Some end_bb} in

				add_terminal (build_stmt scope fmap new_ptj (L.builder_at_end context body_bb) body) build_br_while;

				let bb_arr = L.basic_blocks the_function in
				let n = Array.length bb_arr in
				L.move_block_after (Array.get bb_arr (n - 1)) end_bb;

				ignore(L.build_cond_br bool_val body_bb end_bb while_builder);
				L.builder_at_end context end_bb
			| SBreak -> 
				(match ptj.pos_end_while with
				| Some bb -> ignore (L.build_br bb builder); builder
				| None -> builder)
			| SContinue ->
				(match ptj.pos_begin_while with
				| Some bb -> ignore (L.build_br bb builder); builder
				| None -> builder)
			| SFor(_) -> raise (Failure "no support for for loop")

		in
		let ptj: pos_to_jump = {pos_begin_while = None; pos_end_while = None} in
		(* Build the code for each statement in the function *)
		let func_builder: L.llbuilder = build_stmt fun_scope fmap ptj builder (SBlock sfdecl.sbody) in

		(* Add a return if the last block falls off the end *)
		(* TODO: Different return type *)
		add_terminal func_builder 
		(match sfdecl.srtype with
		| A.Int -> L.build_ret (L.const_int i32_t 0)
		| A.Float -> L.build_ret (L.const_float f64_t 0.0)
		| A.Bool -> L.build_ret (L.const_int i1_t 0)
		| A.String -> L.build_ret (L.const_null str_t)
		| A.Matrix(_) -> L.build_ret (L.const_null (L.pointer_type struct_mat_t))
		| A.Fun(_) as t -> L.build_ret (L.const_null (typ_to_ltype t))
		| A.Void -> L.build_ret_void
		| _ -> L.build_ret_void)

	in

	List.iter (build_function_body global_scope fmap) (main_to_front_list funcs);
	the_module

open Ast

type sexpr = typ * sx
and sslice_expr = sexpr option * sexpr option
and sx =
  | SIntLit of int
  | SBoolLit of bool
  | SFloatLit of float
  | SStrLit of string
  | SVar of string
  | SBinop of sexpr * op * sexpr
  | SUnop of uop * sexpr
  | SCall of string * sexpr list
  | SAssign of sexpr * sexpr
  | SInit of string * sexpr
  | SGetIndex of sexpr * sexpr
  | SGetSlice of sexpr * sslice_expr
  | SMat of sexpr list

type sstmt = 
	| SBlock of sstmt list
  | SExpr of sexpr
  | SReturn of sexpr option
  | SIf of sexpr * sstmt * sstmt
  | SFor of sexpr * sexpr * sexpr * sstmt
  | SWhile of sexpr * sstmt
  | SBreak
  | SContinue

type sfunc_decl = {
  srtype: typ;
  sfname : string;
  sformals : (typ * string) list;
  sbody : sstmt list;
}

(* (include files, semantic checked global variables, semantic checked functions) *)
type sprogram = string list * sexpr list * sfunc_decl list

(* Pretty-printing functions *)

let rec string_of_sexpr (t, e): string =
  "(" ^ string_of_typ t ^ " : " ^ (match e with
        SIntLit(i) -> string_of_int i
      | SBoolLit(true) -> "true"
      | SBoolLit(false) -> "false"
      | SFloatLit(f) -> string_of_float f
      | SStrLit(s) -> s
      | SVar(s) -> s
      | SBinop(e1, o, e2) -> string_of_sexpr e1 ^ " " ^ string_of_op o ^ " " ^ string_of_sexpr e2
      | SUnop(uop, e) -> string_of_uop uop ^ " " ^ string_of_sexpr e
      | SCall(f, el) -> f ^ "(" ^ String.concat ", " (List.map string_of_sexpr el) ^ ")"
      | SAssign(v, e) -> string_of_sexpr v ^ " = " ^ string_of_sexpr e
      | SInit(id, e) -> id ^ " := " ^ string_of_sexpr e
      | SGetIndex(e1, e2) -> string_of_sexpr e1 ^ "[" ^ string_of_sexpr e2 ^ "]"
      | SGetSlice(e, se) -> string_of_sexpr e ^ "[" ^ (string_of_sopt (fst se)) ^ ":" ^ (string_of_sopt (snd se)) ^ "]"
      | SMat(el) -> "[" ^ String.concat ", " (List.map string_of_sexpr el) ^ "]"
    ) ^ ")"
and string_of_sopt (opt: sexpr option): string = 
  match opt with
  | None -> "NONE"
  | Some(e) -> string_of_sexpr e
and string_of_sstmt (sst: sstmt): string = 
  match sst with
    SBlock(stmts) -> "{\n" ^ String.concat "" (List.map string_of_sstmt stmts) ^ "}\n"
  | SExpr(expr) -> string_of_sexpr expr ^ ";\n"
  | SReturn(expr) -> "return " ^ (string_of_sopt expr) ^ ";\n"
  | SIf(e, s1, s2) ->  "if (" ^ string_of_sexpr e ^ ") {\n" ^ string_of_sstmt s1 ^ "} else { \n" ^ string_of_sstmt s2 ^ "\n}\n"
  | SWhile(e, s) -> "while (" ^ string_of_sexpr e ^ ") {" ^ string_of_sstmt s ^ "\n}\n"
  | SFor(e1, e2, e3, s) -> "for (" ^ string_of_sexpr e1 ^ "; " ^ string_of_sexpr e2 ^ "; " ^ string_of_sexpr e3 ^ ") {\n" ^ string_of_sstmt s ^ "\n}\n"
  | SBreak -> "break;"
  | SContinue -> "continue"

let string_of_sfdecl fdecl =
  string_of_typ fdecl.srtype ^ " " ^
  fdecl.sfname ^ "(" ^ String.concat ", " (List.map snd fdecl.sformals) ^ ")\n{\n" ^
  String.concat "" (List.map string_of_sstmt fdecl.sbody) ^ "}\n"

let string_of_sinclude s = "#include <" ^ s ^ ">"

let string_of_sprogram (includes, g_vars, funcs) =
  "\n Sementically checked program: \n" ^
  String.concat "" (List.map string_of_sinclude includes) ^ "\n" ^
  String.concat "" (List.map string_of_sexpr g_vars) ^ "\n" ^
  String.concat "\n" (List.map string_of_sfdecl funcs)
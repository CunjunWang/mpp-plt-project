// a multi-dimensional in matrix
struct Foo {
	int nr_dim;
	int* dim_szs;
	int* start; // point to the first int element
	int length; // point to the last int element
};

void init_foo(struct Foo* foo, int n, ...);
//struct Foo* get_dim(struct Foo* foo);
//struct Foo* get_index(struct Foo* foo, int i); // not indices, one index
// struct Foo* get_slice(struct Foo* foo, int i, int j);

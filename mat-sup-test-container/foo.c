#include "foo.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

void initFoo(struct Foo* foo, int n, ...) {
	int i = 0;
	int sum = 0;
	int total = 1;
	va_list valist;
	va_start(valist, n);

	foo->nr_dim = n;
	foo->dim_szs = (int*) malloc(sizeof(int) * n);
	for (i = 0; i < n; i++) {
		foo->dim_szs[i] = va_arg(valist, int);
	}
	for (i = 0; i < n; i++) {
		//sum+=va_arg(valist, int);
		sum += foo->dim_szs[i];
		total = total * foo->dim_szs[i];
	}
	foo->length = total;
	foo->start = (int*) malloc(sizeof(int) * total);
	va_end(valist);
}
/*
struct Foo* get_dim(struct Foo* foo) {
	int nr_dim = foo->nr_dim;
	struct Foo *result = malloc(sizeof(struct Foo));
	result->nr_dim = 1;
	result->dim_szs = &nr_dim;
	int* start = foo->dim_szs;
	result->start = start;
	result->length = nr_dim;
	return result;
}



struct Foo* getIndex(struct Foo* foo, int index) {
	int nr_dim = foo->nr_dim;
	struct Foo *result = malloc(sizeof(struct Foo));
	result->nr_dim = nr_dim - 1;
	int *s = foo->dim_szs;
	int *st = (int*) malloc(sizeof(int) * (nr_dim - 1));
	for (int i = 0; i < nr_dim-1; i++) {
		st[i] = foo->dim_szs[i+1];
	}
	result->dim_szs = st;
	result->length = foo->length/(*s);
	int* start = foo->start;
	for (int i = 0; i < index; i++) {
		for (int j = 0; j < result->length; j++) {
			start++;
		}
	}
	result->start = start;
	return result;
}

int main(int argc, char const *argv[])
{
	struct Foo f;
	initFoo(&f, 3, 2, 3, 4);
	struct Foo *res = getIndex(&f, 1);
	struct Foo *p = getDim(res);
	printf("%d\n", res->length);
	int *e = res->dim_szs;
	for (int i = 0; i < res->nr_dim; i++) {
		printf("%d\n", *e);
		e++;
	}

	int *r = res->start;
	for (int i = 0; i < res->length; i++) {
		r++;
		printf("%d\n", *r);
	}

	return 0;
}
*/


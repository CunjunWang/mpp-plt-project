open Ast
open Parser

let test_parser source =
  let lexbuf = Lexing.from_string source in
  let program = Parser.program Scanner.token lexbuf in
  print_endline (string_of_program program);;
  (*
  try
    let lexbuf = Lexing.from_string source in
    let program = Parser.program Scanner.token lexbuf in
    print_endline (string_of_program program)
  with Stdlib.Parsing.Parse_error ->
    print_endline ("parser error\n");;
  *)

print_endline "1. test single include.";;
test_parser "include \"builtin.mpp\";";;

print_endline "2. test multiple includes.";;
test_parser "include \"stdio.mpp\";\n include \"builtin.mpp\";";;

print_endline "3. test init expression.";;
test_parser "int abd = 1; \n a
 := 234;";;

print_endline "4. test ignore single line comment.";;
test_parser ("// this is a comment. \n" ^
              "int a = 1;");;

print_endline "5. test ignore multiple lines comment.";;
test_parser ("/*\n" ^
              " * 123\n" ^
              " */\n" ^
              "int b = 2; int a = 1;");;

print_endline "6. test function";;
test_parser ("int func inc(int a) { \n" ^
              " // increment a \n" ^
              " a = a + 1; \n" ^
              " return a; \n" ^
              "}");;

print_endline "7. test while loop";;
test_parser ("void func test() {\n" ^
              "  int i = 10;\n" ^
              "  while (i >= 0) {\n" ^
              "    int j = 0;\n" ^
              "    i = i - 1;\n" ^
              "  }\n" ^
              "  return i;" ^
              "}\n");;

print_endline "8. test for loop";;
test_parser ("void func test() {\n" ^
              "  for (int i = 0; i < 10; i = i + 1) {\n" ^
              "    int j = 0;\n" ^
              "    j = j + 1;\n" ^
              "  }\n" ^
              "  return 1;" ^
              "}\n");;

print_endline "9. test for if else";;
test_parser ("void func test() {" ^
              "  int i = 5;" ^
              "  int j = 0;" ^
              "  if (i > 0) {" ^
              "    j = j + 1;" ^
              "  } else {" ^
              "    j = j - 1;" ^
              "  }" ^
              "  return j;" ^
              "}");;

print_endline "10. test for if else";;
test_parser ("void func test() {" ^
              "  int i = 5;" ^
              "  int j = 0;" ^
              "  if (i > 0) {" ^
              "    j = j + 1;" ^
              "  } else {" ^
              "    j = j - 1;" ^
              "  }" ^
              "  return j;" ^
              "}");;

print_endline "11. test complex program";;
test_parser ("/* The GCD algorithm in MicroC */" ^
              "int a = 19;\n" ^
              "int b = 9;" ^
              "int func gcd(int a, int b) {" ^
              "  while (a != b) {" ^
              "    if (b < a) { a = a - b; }" ^
              "    else { b = b - a; }" ^
              "  }" ^
              "  return a;" ^
              "}" ^
              "int func main() {" ^
              "  int x = 2;" ^
              "  int y = 14;" ^
              "  print(gcd(x,y));" ^
              "  print(gcd(3,15));" ^
              "  print(gcd(99,121));" ^
              "  print(gcd(a,b));" ^
              "  return 0;" ^
              "}");;

print_endline "12. test matrix initialization";;
test_parser ("/* this is a matrix */" ^
              "mat<int,2> m = [[1,2],[3,4]];");;

print_endline "13. test matrix initialization";;
test_parser ("/* this is a matrix */" ^
              "mat<int,2> m = mat(int, 3, 4);");;

print_endline "13. test matrix initialization";;
test_parser ("/* this is a matrix */" ^
              "m := mat(int, 3, 4);");;

print_endline "14. test parse float 1";;
test_parser ("/* this is a matrix */" ^
              "float a = 1.;");;

print_endline "15. test parse float 2";;
test_parser ("/* this is a matrix */" ^
              "float a = -1.25;");;

print_endline "16. test parse float 3";;
test_parser ("/* this is a matrix */" ^
              "float a = .3;");;

print_endline "17. test parse float 4";;
test_parser ("/* this is a matrix */" ^
              "float a = 1e5;");;

print_endline "18. test parse float 5";;
test_parser ("/* this is a matrix */" ^
              "float a = .2e-2;");;

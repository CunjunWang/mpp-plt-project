CC=gcc
CFLAGS= -g -Wall
LDFLAGS= -g

default: mpp.native libmat.a

mpp.native: mpp.ml scanner.mll parser.mly irgen.ml semant.ml ast.ml sast.ml
	ocamlbuild -use-ocamlfind mpp.native -pkgs llvm

libmat.a: mat.o
	ar -crs libmat.a mat.o
	ranlib libmat.a

mat.o: mat.h mat.c

.PHONY: clean
clean:
	ocamlbuild -clean 2>/dev/null
	rm -f scanner.native
	rm -rf _build
	rm -rf *.o *.s *.byte exe *.out *.a mpp.native

.PHONY: all
all: clean default
